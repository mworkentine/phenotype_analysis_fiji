import ij.gui.Wand
from ij.plugin.filter import Binary
from phenotype import phenotype as ph
from os import listdir


def preProcess(image):
    IJ.run(image,"Auto Threshold", "method=Otsu")
 
    for i in range(1,25):
        IJ.run(image,"Erode", "")
    IJ.run(image, "Fill Holes", "")
    for i in range(1,25):
        IJ.run(image,"Dilate", "")


def findCenter(ip):
    '''Finds the center area of the image for autocropping'''

    w = Wand(ip)

    height = ip.getHeight()
    width = ip.getWidth()
    center = (width/2, height/2)

    w.autoOutline(center[0], center[1], 1, 255, w.EIGHT_CONNECTED)

    return PolygonRoi(w.xpoints, w.ypoints, w.npoints, Roi.TRACED_ROI)

def thresholdForColonies(image):
    '''Autothresholds the cropped image to detect the colonies'''
    subtractBackground(image)
    IJ.run(image, "Auto Threshold", "method=Otsu white")
    IJ.run(image, "Open", "")
    IJ.run(image, "Erode", "")
    
def subtractBackground(image):
    '''Substracts the background'''
    subtracter = BackgroundSubtracter()
    ip = image.getProcessor()
    subtracter.rollingBallBackground(ip, 50, False, False, False, False, False)
    image.updateAndDraw()

def autoCrop(image):
    '''tries to autocrop the image to the area with colonies'''
    
    IJ.run(image, "8-bit", "")
    image2 = Duplicator().run(image)

    preProcess(image2)
    roi = findCenter(image2.getProcessor())
        
    image.setRoi(roi)

    if roi.getNCoordinates() != 0:
        IJ.run(image,"To Bounding Box", "")
        IJ.run(image, "Crop", "")
        IJ.run(image, "Enhance Contrast", "saturated=0.35")
    

def main():

    input_folder = ph.get_directory("input")
    output_folder = ph.get_directory("output") 
    file_list = listdir(input_folder)

    for i,imageFile in enumerate(file_list):
        IJ.showProgress(i, len(file_list) + 1)
        file_path = input_folder + imageFile
        image = IJ.openImage(file_path)
        autoCrop(image)
        out_path = output_folder + imageFile
        FileSaver(image).saveAsTiff(out_path)

        image.close()

    IJ.showMessage("All done!")    

def main2():

    image = WindowManager.getCurrentImage()
    autoCrop(image)
    autoCrop(image)

if __name__ == '__main__':
    main()


