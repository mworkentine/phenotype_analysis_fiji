###############
# HCN Process #
###############

""" Jython script to automatically process HCN images.  Assumes the
image has been cropped correctly.  Saves the output to a text file
with the same name as the image file.
"""


from phenotype import phenotype as ph
from os import listdir


def main():

    # get the input and output directorys
    input_folder = ph.get_directory("input")
    output_folder = ph.get_directory("output")

    # get file list from input folder
    file_list = listdir(input_folder)
    if not file_list:
        IJ.error("No files in directory.  Exiting...")
        sys.exit()

    # set the row and col, 384 only for now
    numRow = 16
    numCol = 24

    ## --- for debugging in ImageJ
    # image = WindowManager.getCurrentImage()
    # image2 = Duplicator().run(image)

    for imageFile in file_list:

        # open the image and get some info
        file_path = input_folder + imageFile
        image = IJ.openImage(file_path)
        imageName = image.getShortTitle()
        
        # pre-process and measure spots
        ph.hcn_process(image)
        measurements = ph.measure_image(image, numRow, numCol, "mean")

        # save the results
        resultsPath = output_folder + imageName + "_results.txt"
        resultsFile = open(resultsPath, "w")
        
        try:
            resultsFile.write("Well\tValue\n")
            for spot in measurements:
                resultsFile.write(spot[0] + "\t" + str(spot[1]) + "\n")
        finally:
            resultsFile.close()

        image.changes = False
        image.close()

    IJ.showMessage("All done!") 



if __name__ == '__main__':
    main() 