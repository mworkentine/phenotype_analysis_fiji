##################
# Command Center #
##################

"""Jython script for running the various phenotyp analysis scripts for the
Pa diversity project.  Run from within Fiji. """

from ij import IJ
from ij import WindowManager


from java.awt import FlowLayout
from java.awt import Dimension
from javax.swing import BoxLayout
from javax.swing import Box
from javax.swing import JFrame
from javax.swing import JButton
from javax.swing import JPanel
from javax.swing import JLabel
from javax.swing import JTextField
from java.awt import GridLayout

from java.lang.System import getProperty
import sys
sys.path.append(getProperty("fiji.dir") + "/plugins/phenotype_analysis_fiji/")

import phenotype as ph
import traceback


class CommandCenter(JFrame):

    """Class for setting up the Command Center interface"""

    def __init__(self):
        super(CommandCenter, self).__init__()
        self.initUI()

    def initUI(self):

        # Define some panels
        # -------------------

        # main panel
        panel = JPanel()
        self.getContentPane().add(panel)
        panel.setLayout(BoxLayout(panel, BoxLayout.Y_AXIS))

        # misc functions/buttons
        misc = JPanel()
        misc.setLayout(GridLayout(5, 2, 2, 2))
        rotateButtons = JPanel()
        rotateButtons.setLayout(GridLayout(1, 2, 2, 2))
        gridButtons = JPanel()
        gridButtons.setLayout(GridLayout(1, 2, 2, 2))

        # buttons for swimming motility
        swim = JPanel()
        swim.setLayout(GridLayout(4, 2, 2, 2))
        #swimRotate = JPanel()
        # swimRotate.setLayout(GridLayout(1,2,2,2))

        # panel for phenotype buttons
        phenotypes = JPanel()
        phenotypes.setLayout(GridLayout(2, 3, 2, 2))

        # panel for the cropping box sizer
        boxSizer = JPanel()
        boxSizer.setLayout(FlowLayout(FlowLayout.LEADING, 2, 2))

        # Define all the components to go in the panels
        setScaleGDoc = JButton(
            "Set Scale GDoc", actionPerformed=self.onScaleGDoc)
        setScale = JButton("Set Scale", actionPerformed=self.onSetScale)
        crop = JButton("Crop", actionPerformed=self.onCrop)
        contrast = JButton("Contrast", actionPerformed=self.onContrast)
        convert = JButton("8-bit", actionPerformed=self.onConvert)
        saveAsTiff = JButton("Save as Tiff", actionPerformed=self.onSaveAsTiff)
        saveImgSeq = JButton(
            "Save image sequence", actionPerformed=self.onSaveImgSeq)
        openImage = JButton("Open image", actionPerformed=self.onOpenImg)
        importImgSeq = JButton(
            "Import image sequence", actionPerformed=self.onImportImgSeq)
        deleteSlice = JButton(
            "Delete slice", actionPerformed=self.onDeleteSlice)

        getMeasurement = JButton(
            "Get measurements", actionPerformed=self.onGetMeasurement)

        markNonSwimmers = JButton(
            "Mark non-swimmers", actionPerformed=self.onNonSwimmers)

        getSwimMeasurements = JButton(
            "Swim Measurments", actionPerformed=self.onGetSwimMeasurements)

        rotateLeftStack = JButton(
            "Stack R-Left", actionPerformed=self.onRotateLeftStack)
        rotateRightStack = JButton(
            "Stack R-Right", actionPerformed=self.onRotateRightStack)

        swimStack = JButton("Analyze Swim", actionPerformed=self.onSwimStack)
        analyze = JButton("Analyze image", actionPerformed=self.onAnalyze)

        setRect = JButton("Set Box", actionPerformed=self.onSetRect)
        rotateLeft = JButton("R-Left", actionPerformed=self.onRotateLeft)
        rotateRight = JButton("R-Right", actionPerformed=self.onRotateRight)
        grid96 = JButton("96-grid", actionPerformed=self.onGrid96)
        grid384 = JButton("384-grid", actionPerformed=self.onGrid384)

        self.boxWidth = JTextField('1530', 4)
        self.boxHeight = JTextField('1020', 4)

        boxSizer.add(JLabel("X:", JLabel.RIGHT))
        boxSizer.add(self.boxWidth)
        boxSizer.add(JLabel("Y:", JLabel.RIGHT))
        boxSizer.add(self.boxHeight)
        boxSizer.add(setRect)

        rotateButtons.add(rotateLeft)
        rotateButtons.add(rotateRight)
        gridButtons.add(grid96)
        gridButtons.add(grid384)

        misc.add(openImage)
        misc.add(saveAsTiff)
        misc.add(setScaleGDoc)
        misc.add(setScale)
        misc.add(crop)
        misc.add(contrast)
        misc.add(convert)
        misc.add(boxSizer)
        misc.add(rotateButtons)
        misc.add(gridButtons)

        # swimRotate.add(rotateLeftStack)
        # swimRotate.add(rotateRightStack)

        swim.add(importImgSeq)
        swim.add(saveImgSeq)
        swim.add(rotateLeftStack)
        swim.add(rotateRightStack)
        swim.add(deleteSlice)
        swim.add(swimStack)
        swim.add(markNonSwimmers)
        swim.add(getSwimMeasurements)

        phenotypes.add(analyze)
        phenotypes.add(getMeasurement)

        miscLabel = JLabel("Miscellaneous pre-processing")
        miscLabel.setAlignmentX(miscLabel.CENTER_ALIGNMENT)

        swimLabel = JLabel("Swimming motility analysis")
        swimLabel.setAlignmentX(swimLabel.CENTER_ALIGNMENT)

        phenotypeLabel = JLabel("Phenotype analysis")
        phenotypeLabel.setAlignmentX(phenotypeLabel.CENTER_ALIGNMENT)

        panel.add(miscLabel)
        panel.add(misc)
        panel.add(Box.createRigidArea(Dimension(0, 15)))
        panel.add(swimLabel)
        panel.add(swim)
        panel.add(Box.createRigidArea(Dimension(0, 15)))
        panel.add(phenotypeLabel)
        panel.add(phenotypes)
        panel.add(Box.createRigidArea(Dimension(0, 15)))

        # start it up
        self.setTitle("Command Center")
        self.setSize(500, 500)
        self.setLocationRelativeTo(None)
        self.setVisible(True)

    # --- button actions
    def onSwimStack(self, event):
        try:
            IJ.run("Swim analyze")
        except:
            IJ.log(traceback.format_exc())

    def onAnalyze(self, event):
        ph.run_profiles()

    def onScaleGDoc(self, event):
        image = IJ.getImage()
        ph.set_scale(image)

    def onSetScale(self, event):
        IJ.run("Set Scale...")

    def onNonSwimmers(self, event):
        image = WindowManager.getCurrentImage()
        try:
            ph.get_measurements(image, 'noSwim')
        except:
            IJ.log(traceback.format_exc())

    def onCrop(self, event):
        IJ.run("Crop")

    def onConvert(self, event):
        IJ.run("8-bit")

    def onContrast(self, event):
        IJ.run("Enhance Contrast", "saturated=0.35")

    def onSaveAsTiff(self, event):
        image = WindowManager.getCurrentImage()
        IJ.saveAsTiff(image, "")

    def onSaveImgSeq(self, event):
        IJ.run("Image Sequence... ")

    def onGetMeasurement(self, event):
        image = WindowManager.getCurrentImage()

        try:
            ph.get_measurements(image, 'swim')
        except:
            IJ.log(traceback.format_exc())

    def onGetSwimMeasurements(self, event):
        image = WindowManager.getCurrentImage()
        try:
            ph.get_measurements(image, 'swim')
        except:
            IJ.log(traceback.format_exc())

    def onDeleteSlice(self, event):
        IJ.run("Delete Slice")

    def onImportImgSeq(self, event):
        IJ.run("Image Sequence...")

    def onOpenImg(self, event):
        IJ.open()

    def onRotateLeft(self, event):
        IJ.run("Rotate... ", "angle=-0.5 grid=1 interpolation=Bilinear")

    def onRotateRight(self, event):
        IJ.run("Rotate... ", "angle=0.50 grid=1 interpolation=Bilinear")

    def onRotateLeftStack(self, event):
        IJ.run("Rotate... ", "angle=-0.5 grid=1 interpolation=Bilinear stack")

    def onRotateRightStack(self, event):
        IJ.run("Rotate... ", "angle=0.50 grid=1 interpolation=Bilinear stack")

    def onGrid96(self, event):
        image = IJ.getImage()
        ph.ROI_grid(image, 8, 12)

    def onGrid384(self, event):
        image = IJ.getImage()
        ph.ROI_grid(image, 16, 24)

    def onSetRect(self, event):
        width = int(self.boxWidth.text)
        height = int(self.boxHeight.text)
        image = WindowManager.getCurrentImage()
        if image is None:
            IJ.error("No images open!")
        else:
            ph.drawBoundBox(image, width, height)


if __name__ == '__main__':
    CommandCenter()
