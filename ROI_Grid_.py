# Plugin to make an ROI grid

from java.lang.System import getProperty
import sys
sys.path.append(getProperty("fiji.dir") + "/plugins/Phenotypes/")
import phenotype as ph
from ij import IJ

image = IJ.getImage()

# get options
grid_size = ph.getGridSize()
if grid_size is not None:
	numRow, numCol = grid_size

ph.ROI_grid(image, numRow, numCol)















		
