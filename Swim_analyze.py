#######################
# Measure Swim Plates #
#######################

# Author: Matthew L. Workentine
# Created on: November 7, 2013
# Version 2

"""
This script, run from within Fiji, takes a stack of images from swimming
motility plates and calculates the area of swimming zone for each isolate on
the plate.  This is version 2, written in Python, with some modifications from
the first version, which was written in the ImageJ macro language.
"""

from phenotype import phenotype as ph
from fiji.threshold import Auto_Threshold
from ij.plugin.filter import Binary
from ij.plugin.filter import EDM
from ij.plugin import Duplicator
from ij import WindowManager
from ij.plugin.frame import RoiManager
from ij import IJ


#-------------------------------
# Function definitions
# ------------------------------


def preProcess(image):
    '''Cleans up the image and preps for processing'''

    stacks = image.getStackSize()
    #IJ.run(image, "Fast Filters",
    #       "link filter=mean x=10 y=10 preprocessing=none offset=128 stack")
    IJ.run(image, "Mean...", "radius=10 stack")
    for i in range(1, stacks):
        image.setSliceWithoutUpdate(i + 1)
        IJ.run(image, "Enhance Local Contrast (CLAHE)",
               "blocksize=500 histogram=256 maximum=2 mask=*None* fast_(less_accurate)")
    # IJ.run(image, "Fast Filters",
    #        "link filter=median x=30 y=30 preprocessing=none offset=128 stack")
    IJ.run(image, "Median...", "radius=5 stack")


def threshold(image):
    '''Auto threshold the image'''

    th = Auto_Threshold()

    method = "Triangle"
    noWhite = False  # ignore white
    noBlack = False  # ignore black
    doIwhite = True  # white objects on black background
    doISet = False  # use setThreshold
    doIlog = False  # log
    doIStackHistogram = True  # use stack histogram

    th.exec(image, method, noWhite, noBlack, doIwhite, doISet,
            doIlog, doIStackHistogram)


def binProcess(image):
    """Binary processing to remove more noise and artifacts"""

    stack = image.getStack()
    binner = Binary()

    erode = 1
    dilate = 1

    for i in range(1, stack.getSize() + 1):
        ip = stack.getProcessor(i)

        binner.setup("dilate", image)
        for i in range(1, dilate + 1):
            binner.run(ip)

        binner.setup("erode", image)
        for i in range(1, erode + 1):
            binner.run(ip)

        # fill holes
        binner.setup("fill", image)
        binner.run(ip)

        # watershed
        edm = EDM()
        edm.setup("watershed", image)
        edm.run(ip)

    # def fit_rois(image, manager, rowCoords, colCoords):
    # """ Takes a set of ROI's from the roi manager and fits them to circles
    # Also adds labels and removes out of bound rois.
    # """

    # roiCount = manager.getCount()

    # for i in range(roiCount):
    #     manager.select(0)  # select roi from top of the list
    #     IJ.run("Fit Circle")
    #     curRoi = image.getRoi()
    #     coords = ph.get_roi_center(curRoi.getBounds())
    #     location = ph.get_location(coords, rowCoords, colCoords)
        
    #     if location:
    #         curRoi.setName(str(location))
    #         manager.runCommand("Add") # add new roi to manager
          
        
    #     manager.select(0) # select the top of the list again
    #     manager.runCommand("Delete") # delete the old roi

def main():

    show = False
	
    image1 = WindowManager.getCurrentImage()
    image2 = Duplicator().run(image1)

    spotLabels, numRow, numCol, sample, media, replicate = ph.get_options(
        5, "sample", "Swim", "1")
    rowCoords, colCoords = ph.make_grid_location(
        image2, spotLabels, numRow, numCol)

    if RoiManager.getInstance() is None:
        manager = RoiManager()
    else:
        RoiManager.getInstance().close()
        manager = RoiManager()

    IJ.showStatus("Pre-processing image...")
    preProcess(image2)

    if show:
        postProcess = Duplicator().run(image2)
        postProcess.setTitle("Post Process")
        postProcess.show()
    
    IJ.showStatus("Thresholding...")
    threshold(image2)

    if show:
        postThreshold = Duplicator().run(image2)
        postThreshold.setTitle("Post Threshold")
        postThreshold.show()

    IJ.showStatus("Binary processing...")
    #binProcess(image2)

    if show:
        postBin = Duplicator().run(image2)
        postBin.setTitle("Post Binary")
        postBin.show()

    IJ.showStatus("Analyzing Particles...")
    ph.analyze_particles(image2, 1.5, 100000, 0.3, 1.0, stack=True)

    WindowManager.setWindow(WindowManager.getWindow(image1.getTitle()))
    
    IJ.showStatus("Fitting Rois....")
    ph.fit_rois(image1, manager, rowCoords, colCoords)

    IJ.showMessage("All Done!")



if __name__ == '__main__':
    main()
