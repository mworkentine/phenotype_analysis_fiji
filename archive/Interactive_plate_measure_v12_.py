##################################
# Interactive Plate measurements #
##################################

# Author: Matthew L. Workentine
# Created on May 22, 2013
# Version 1.2

"""
Semi-automated measurement of 96-well phenotype plates. 
In order for the measurements to be accurate be sure to set the 
scale in ImageJ prior to running the script.

This version removes the need for adding in the 'zero' values manually.
The location of roi is determined and labeled automatically.  Also 
assigns the correct labels based on stamp number.
"""

from java.awt.event import KeyEvent, KeyAdapter
from ij.gui import GenericDialog
import string
import sys
import re

#-------------------------------
# Class and function definitions
# ------------------------------

class ListenToKey(KeyAdapter):
  def keyPressed(this, event):
    imp = event.getSource().getImage()
    doSomething(imp, event)

def doSomething(imp, keyEvent):
  """ A function to react to key being pressed on an image canvas. """
  if keyEvent.getKeyCode() == 70: # 'f' key
  	measureSpot()
  	# Prevent further propagation of the key event:
  	keyEvent.consume()
  elif keyEvent.getKeyCode() == 83: # 's' key
  	markAsZero()
  	keyEvent.consume()
  elif keyEvent.getKeyCode() == 68: # 'd' key
  	markNoGrowth()
  	keyEvent.consume()

def getOptions():
	stamps = ("1","2","3","4")
	gd = GenericDialog("Plate Options")
	gd.addChoice("Select stamp number", stamps, stamps[0])
	gd.addMessage("Command keys:\n[f]: Measure\n[d]: Mark No_growth\n[s]: Mark as '0'")
	gd.hideCancelButton()
	gd.showDialog()
	stamp = gd.getNextChoice()
	return int(stamp)

def recordValue(diameter, selection):
	"""Sets the specified diameter of selection in the results table"""
	
	coords = getRoiCenter(selection.getBounds())
	currentLabel = getLocation(coords)
	listResults = getResultsList()

		# if the spot has alredy been measured then replace with new value
	if currentLabel in listResults:
		Rt.setValue("Diameter", listResults.index(currentLabel), diameter)
	else:
		Rt.incrementCounter()
		Rt.addValue("Diameter", diameter)
		# add the well label
		Rt.addLabel(currentLabel)

	Rt.show(resultsTitle)
		
def measureSpot():
	""" Measure the roi diameter and add to results table.  Adds 0 if no
	    roi is selected. """

	selection = image.getRoi()

	if selection is not None:
		diameter = selection.getFeretsDiameter()
		recordValue(diameter, selection)
	else:
		return

def markAsZero():
	"""Mark the current selection as zero"""
	selection = image.getRoi()

	if selection is not None:
		diameter = 0
		recordValue(diameter, selection)
	else:
		return

def markNoGrowth():
	"""Mark the current selection as no growth"""
	selection = image.getRoi()

	if selection is not None:
		diameter = "No_growth"
		recordValue(diameter, selection)
	else:
		return
		
def makeGrid():
	"""
	Makes the 8x12 grid and returns two dictionarys of well labels as keys
	and grid coordinates.  First is for the rows and second is for columns.
	Only for 96 well plates.
	"""
	
	# get image height and width
	ip = image.getProcessor()
	imgHeight = ip.getHeight()
	imgWidth = ip.getWidth()
	
	# set paramters
	height = imgHeight / numRow
	width  = imgWidth / numCol

	# make the labels based on number of columns and rows
	regex = re.compile("([A-P])(\d*)")
	letters = list()
	numbers = list()
	for spot in spotLabels:
		label = regex.search(spot)
		letters.append(label.groups()[0])
		numbers.append(label.groups()[1])

	letters = list(set(letters))
	letters.sort()
	numbers = list(set(numbers))
	numbers = sorted(numbers, key = int)

	# row coordinates
	rowCoords = {}
	for i in range(0, numRow):
		y1 = (i * height)
		y2 = y1 + (height - 1)
		rowCoords[letters[i]] = (y1,y2)

	# column coordinates
	colCoords = {}
	for j in range(0, numCol):
		x1 = (j * width)
		x2 = x1 + (width - 1)
		colCoords[numbers[j]] = (x1,x2)	

	return rowCoords, colCoords

def getLocation(coords):
	""" Get the grid square location of a point (row, col) in image based on the coordinates
		in gridRows and gridCols which are dictionaries
	"""
	gridRows, gridCols = makeGrid()
	
	# get row value
	for key in gridRows:
		if gridRows[key][0] <= coords[0] <= gridRows[key][1]:
			rowValue = key

	# get col value
	for key in gridCols:
		if gridCols[key][0] <= coords[1] <= gridCols[key][1]:
			colValue = key
	try:
		rowValue, colValue
	except NameError:
		IJ.error("Coordinates out of range.\n No location found for this point!!")
		sys.exit()

	return rowValue + colValue

def getRoiCenter(roiBounds):
	"""Get the center of roi based on given bounds"""
	x = roiBounds.x
	y = roiBounds.y
	height = roiBounds.height
	width = roiBounds.width

	center_x = x + (width / 2)
	center_y = y + (height / 2)

	return center_y, center_x


def getResultsList():
	""" Returns the labels present in the Results list"""
	numResults = Rt.getCounter()

	labels = list()
	for r in range(0, numResults):
		labels.append(Rt.getLabel(r))

	return labels
						
  	
def getPlateLabels(stamp):
	"""Gets the correct labels for the current image based on the plate stamping"""
	
	indexRanges = {1:range(0,337, 48), 2:range(1,338, 48), 3:range(25,362, 48), 4:range(24,361, 48)}
	
	letters = string.ascii_uppercase[0:16]
	numbers = map(str, range(1,24 + 1))
	full_labels = [(i + j) for i in letters for j in numbers]


	index = [(full_labels[j]) for i in indexRanges[stamp] for j in range(i, i+23, 2)]
	return index

	

# ------------------------
# Set up the key listener
# -----------------------

# create listener
listener = ListenToKey()

# get current image, window and canvas
image = WindowManager.getCurrentImage()
win = image.getWindow()
canvas = win.getCanvas()

# Remove existing key listeners
kls = canvas.getKeyListeners()
map(canvas.removeKeyListener, kls)

# Add our key listener
canvas.addKeyListener(listener)
  
# ----------------------------------------------------
# get some info about the image and set up parameters
# ----------------------------------------------------

imageName = image.getShortTitle()
resultsTitle = imageName + "_results"
numRow = 8
numCol = 12


# intialize a new results table
Rt = ResultsTable()

# ---------------------------------
# Get plate stamp number from user
# ---------------------------------

stampNum = getOptions()
spotLabels = getPlateLabels(stampNum)



	