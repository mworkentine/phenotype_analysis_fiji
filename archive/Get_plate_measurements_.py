###########################################
# Get plate measurements from ROI manager #
###########################################

# Author: Matthew L. Workentine
# Created on September 18, 2013
# Version 1.0

"""
Takes the list of ROI's from the ROI manager, assigns well labels based on
user given plate paramters and returns a set of measurements.
"""

from ij.gui import GenericDialog
import string
import sys
import re

#-------------------------------
# Function definitions
# ------------------------------


def getOptions():
    stamps = ("1","2","3","4")
    gd = GenericDialog("Plate Options")
    gd.addRadioButtonGroup("Select stamp number", stamps, 1, 4, stamps[0])
    gd.hideCancelButton()
    gd.showDialog()
    stamp = gd.getNextRadioButton()
    return int(stamp)


def recordValue(diameter, selection):
    """Sets the specified diameter of selection in the results table"""
    
    coords = getRoiCenter(selection.getBounds())
    currentLabel = getLocation(coords)
            
    Rt.incrementCounter()
    Rt.addValue("Diameter", diameter)
    # add the well label
    Rt.addLabel(currentLabel)


            
def makeGrid():
    """
    Makes the 8x12 grid and returns two dictionarys of well labels as keys
    and grid coordinates.  First is for the rows and second is for columns.
    Only for 96 well plates.
    """
    
    # get image height and width
    ip = image.getProcessor()
    imgHeight = ip.getHeight()
    imgWidth = ip.getWidth()
    
    # set paramters
    height = imgHeight / numRow
    width  = imgWidth / numCol

    # make the labels based on number of columns and rows
    regex = re.compile("([A-P])(\d*)")
    letters = list()
    numbers = list()
    for spot in spotLabels:
        label = regex.search(spot)
        letters.append(label.groups()[0])
        numbers.append(label.groups()[1])

    letters = list(set(letters))
    letters.sort()
    numbers = list(set(numbers))
    numbers = sorted(numbers, key = int)

    # row coordinates
    rowCoords = {}
    for i in range(0, numRow):
        y1 = (i * height)
        y2 = y1 + (height - 1)
        rowCoords[letters[i]] = (y1,y2)

    # column coordinates
    colCoords = {}
    for j in range(0, numCol):
        x1 = (j * width)
        x2 = x1 + (width - 1)
        colCoords[numbers[j]] = (x1,x2) 

    return rowCoords, colCoords

def getLocation(coords):
    """ Get the grid square location of a point (row, col) in image based on the coordinates
        in gridRows and gridCols which are dictionaries
    """
    gridRows, gridCols = makeGrid()
    
    # get row value
    for key in gridRows:
        if gridRows[key][0] <= coords[0] <= gridRows[key][1]:
            rowValue = key

    # get col value
    for key in gridCols:
        if gridCols[key][0] <= coords[1] <= gridCols[key][1]:
            colValue = key
    try:
        rowValue, colValue
    except NameError:
        IJ.error("Coordinates out of range.\n No location found for this point!!")
        sys.exit()

    return rowValue + colValue

def getRoiCenter(roiBounds):
    """Get the center of roi based on given bounds"""
    x = roiBounds.x
    y = roiBounds.y
    height = roiBounds.height
    width = roiBounds.width

    center_x = x + (width / 2)
    center_y = y + (height / 2)

    return center_y, center_x

def getResultsList():
    """ Returns the labels present in the Results list"""
    numResults = Rt.getCounter()

    labels = list()
    for r in range(0, numResults):
        labels.append(Rt.getLabel(r))

    return labels
                        
    
def getPlateLabels(stamp):
    """Gets the correct labels for the current image based on the plate stamping"""
    
    indexRanges = {1:range(0,337, 48), 2:range(1,338, 48), 3:range(25,362, 48), 4:range(24,361, 48)}
    
    letters = string.ascii_uppercase[0:16]
    numbers = map(str, range(1,24 + 1))
    full_labels = [(i + j) for i in letters for j in numbers]


    index = [(full_labels[j]) for i in indexRanges[stamp] for j in range(i, i+23, 2)]
    return index

def get_stamp_from_name(imageName):
    """ Function to extract the stamp number from the image name """

    # regex to get info from file name
    regex = re.compile("^([A-Z][0-9][a-d]?)[-_]([a-zA-Z]+)([0-9])[-_]S([1,2,3,4])")
    hits = regex.search(imageName)
    if hits:
        return hits.groups()[3]
    else:
        return None



def main():

    # ----------------------------------------------------
    # get some info about the image and set up parameters
    # ----------------------------------------------------

    global image
    global numRow
    global numCol
    global spotLabels
    global Rt
    
    image = WindowManager.getCurrentImage()
    imageName = image.getShortTitle()
    resultsTitle = imageName + "_results"
    numRow = 8
    numCol = 12

    # intialize a new results table
    Rt = ResultsTable()

    # ---------------------------------
    # Get plate stamp number from user
    # ---------------------------------

    stampNum = get_stamp_from_name(imageName)

    if not stampNum:
        stampNum = getOptions()
    
    stampNum = int(stampNum)
    spotLabels = getPlateLabels(stampNum)

    msg = "Using stamp %s" % stampNum
    IJ.showStatus(msg)
    
    # get measurements

    manager = RoiManager.getInstance()
    roiList = manager.getRoisAsArray()
    
    for roi in roiList:
    	image.setRoi(roi)
    	measurements = Analyzer(image, Measurements.AREA | Measurements.FERET, Rt)
    	try:
            diameter = roi.getFeretsDiameter()
        except:
            diameter = 0
        recordValue(diameter, roi)
        

    Rt.show(resultsTitle)

if __name__ == '__main__':
    main()
