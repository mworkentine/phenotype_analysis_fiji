""" This script automatically detects protease zones of clearing on skim milk 
plates.  Run this script from inside Fiji.
"""

from phenotype import phenotype as ph


def main():
    
    # get the open image
    image1 = WindowManager.getCurrentImage()

    # convert to 8-bit
    #IJ.run(image1, "8-bit")

    # duplicate for processing
    image2 = Duplicator().run(image1)
    
    # get an ROI manager instance if running or start new one if not
    if RoiManager.getInstance() is None:
        manager = RoiManager()
    else:
        manager = RoiManager.getInstance()

    # smooth with Gaussian blur
    IJ.run(image2, "Gaussian Blur...", "sigma=2")

    # remove artifacts
    IJ.run(image2,"Remove Outliers...", "radius=10 threshold=1 which=Bright")

    # FTT bandpass filter to correct shading
    IJ.run(image2, "Bandpass Filter...", \
        "filter_large=500 filter_small=0 suppress=None tolerance=5 autoscale saturate")
    image2.show()
    # Auto threshold and invert result
    IJ.run(image2, "Auto Threshold", "method=Otsu white")
    IJ.run(image2, "Invert", "")


    # Process the binary image
    # Two dialation steps helps to get rid of any remaing artifacts
    IJ.run(image2, "Dilate", "")

        # splits any connected zones
    IJ.run(image2, "Watershed", "")

    # This is the key step to get rid of the colony center
    IJ.run(image2, "Fill Holes", "")

    # analyze particles
    ph.analyze_particles(image2, minsize=5, maxsize=500, mincirc=0.5, maxcirc=1)

    # fit the roi's
    #ph.fit_rois(manager, image2)

if __name__ == '__main__':
    main()