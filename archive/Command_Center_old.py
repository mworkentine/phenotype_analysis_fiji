##################
# Command Center #
##################

''' Jython script for running the various phenotyp analysis scripts for the 
Pa diversity project.  Run from within Fiji.
'''

from javax.swing import GroupLayout
from javax.swing import JFrame
from javax.swing import JButton
from javax.swing import JPanel
from javax.swing import BoxLayout
from javax.swing import Box
from javax.swing import JRadioButton
from javax.swing import JSeparator
from javax.swing import JLabel
from javax.swing import LayoutStyle
from javax.swing import ButtonGroup
from javax.swing import SwingConstants
import sys


class CommandCenter(JFrame):
    """Class for setting up the Command Center interface"""
    
    def __init__(self):
        super(CommandCenter,self).__init__()
        self.initUI()

    def initUI(self):

        buttonGroup1 = ButtonGroup()
        Pre_process = JPanel()
        jLabel1 = JLabel()
        setScale = JButton(actionPerformed = self.onScale)
        rotate = JButton(actionPerformed = self.onRotate)
        crop = JButton(actionPerformed = self.onCrop)
        contrast = JButton(actionPerformed = self.onContrast)
        convert = JButton(actionPerformed = self.onConvert)
        saveAsTiff = JButton(actionPerformed = self.onSaveAsTiff)
        saveImgSeq = JButton(actionPerformed = self.onSaveImgSeq)
        openImage = JButton(actionPerformed = self.onOpenImg)
        importImgSeq = JButton(actionPerformed = self.onImportImgSeq)
        deleteSlice = JButton(actionPerformed = self.onDeleteSlice)
        getMeseasurements = JPanel()
        getMeasurement = JButton(actionPerformed = self.onGetMeasurement)
        getMeasurementStack = JButton(actionPerformed = self.onGetMeasurementsStack)
        markNonSwimmers = JButton(actionPerformed = self.onNonSwimmers)
        jLabel3 = JLabel()
        measure = JPanel()
        swimStack = JButton(actionPerformed = self.onSwimStack)
        analyze = JButton(actionPerformed = self.onAnalyze)
        jLabel2 = JLabel()
        enlarge5 = JButton(actionPerformed = self.onEnlarge5)
        shrink5 = JButton(actionPerformed = self.onShrink5)
        grid96 = JButton(actionPerformed = self.onGrid96)
        grid384 = JButton(actionPerformed = self.onGrid384)


        Pre_process.setName("Pre-process") 
        jLabel1.setText("Pre-process")
        setScale.setText("Set Scale (q)")
        rotate.setText("Rotate (r)")
        crop.setText("Crop")
        crop.setToolTipText("")
        contrast.setText("Contrast")
        convert.setText("8-bit")
        saveAsTiff.setText("Save as Tiff")
        saveImgSeq.setText("SaveAs Seq")
        openImage.setText("Open Image")
        importImgSeq.setText("Import Seq")
        deleteSlice.setText("Delete Slice")
        getMeasurement.setText("Get Measurements (F2)")
        getMeasurementStack.setText("Get Measurments Stack (F3)")
        markNonSwimmers.setText("Mark non-Swimmers")
        jLabel3.setText("Get Measurements")
        swimStack.setText("Swim Stack")
        analyze.setText("Phenotype analyze (F1)")
        jLabel2.setText("Measure Zones")
        enlarge5.setText("+5px")
        shrink5.setText("-5px")
        grid96.setText("96 Grid")
        grid384.setText("384 Grid")

        setScale.setToolTipText("Set the image scale.  Assumes a line roi of 20 mm has been drawn.")
        saveImgSeq.setToolTipText("")
        rotate.setToolTipText("")
        markNonSwimmers.setToolTipText("")

        getMeseasurementsLayout = GroupLayout(getMeseasurements)
        getMeseasurements.setLayout(getMeseasurementsLayout)
        getMeseasurementsLayout.setHorizontalGroup(
            getMeseasurementsLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(getMeseasurementsLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(getMeseasurementsLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
                    .addGroup(getMeseasurementsLayout.createSequentialGroup()
                        .addComponent(jLabel3)
                        .addContainerGap(70, Short.MAX_VALUE))
                    .addGroup(GroupLayout.Alignment.TRAILING, getMeseasurementsLayout.createSequentialGroup()
                        .addGroup(getMeseasurementsLayout.createParallelGroup(GroupLayout.Alignment.TRAILING)
                            .addComponent(markNonSwimmers, GroupLayout.Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 184, Short.MAX_VALUE)
                            .addComponent(getMeasurement, GroupLayout.Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 184, Short.MAX_VALUE)
                            .addComponent(getMeasurementStack, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(22, 22, 22))))
        )
        getMeseasurementsLayout.setVerticalGroup(
            getMeseasurementsLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(getMeseasurementsLayout.createSequentialGroup()
                .addComponent(jLabel3)
                .addGap(12, 12, 12)
                .addComponent(getMeasurementStack)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(getMeasurement)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(markNonSwimmers)
                .addContainerGap(28, Short.MAX_VALUE))
        )



        measureLayout = GroupLayout(measure)
        measure.setLayout(measureLayout)

        measureLayout.setHorizontalGroup(
            measureLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(measureLayout.createSequentialGroup()
                .addComponent(jLabel2)
                .addContainerGap(27, Short.MAX_VALUE))
            .addGroup(measureLayout.createSequentialGroup()
                .addGroup(measureLayout.createParallelGroup(GroupLayout.Alignment.TRAILING, False)
                    .addComponent(swimStack, GroupLayout.Alignment.LEADING, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(analyze, GroupLayout.Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 99, Short.MAX_VALUE))
                .addContainerGap(23, Short.MAX_VALUE))
        )
        measureLayout.setVerticalGroup(
            measureLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(measureLayout.createSequentialGroup()
                .addComponent(jLabel2)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(analyze)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(swimStack)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addContainerGap())
        )


        Pre_processLayout = GroupLayout(Pre_process)
        Pre_process.setLayout(Pre_processLayout)

        Pre_processLayout.setHorizontalGroup(
            Pre_processLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(Pre_processLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(Pre_processLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
                    .addGroup(Pre_processLayout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addContainerGap(285, Short.MAX_VALUE))
                    .addGroup(GroupLayout.Alignment.TRAILING, Pre_processLayout.createSequentialGroup()
                        .addGroup(Pre_processLayout.createParallelGroup(GroupLayout.Alignment.TRAILING)
                            .addGroup(GroupLayout.Alignment.LEADING, Pre_processLayout.createSequentialGroup()
                                .addComponent(measure, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(Pre_processLayout.createParallelGroup(GroupLayout.Alignment.LEADING, False)
                                    .addComponent(getMeseasurements, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                            .addGroup(GroupLayout.Alignment.LEADING, Pre_processLayout.createSequentialGroup()
                                .addGroup(Pre_processLayout.createParallelGroup(GroupLayout.Alignment.LEADING, False)
                                    .addComponent(importImgSeq, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(saveImgSeq, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(saveAsTiff, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(openImage, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addGap(44, 44, 44)
                                .addGroup(Pre_processLayout.createParallelGroup(GroupLayout.Alignment.LEADING, False)
                                    .addComponent(setScale, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(contrast, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(rotate, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(grid96, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                                    .addComponent(enlarge5, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(Pre_processLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
                                    .addComponent(convert, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(deleteSlice, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(crop, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(grid384, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(shrink5, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                        .addContainerGap())))
        )
        
        Pre_processLayout.linkSize(SwingConstants.HORIZONTAL, \
            [setScale, contrast, rotate, crop, convert, deleteSlice, grid96, grid384, enlarge5, shrink5])


        Pre_processLayout.setVerticalGroup(
            Pre_processLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(Pre_processLayout.createSequentialGroup()
                .addComponent(jLabel1)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(Pre_processLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                    .addComponent(openImage)
                    .addComponent(setScale)
                    .addComponent(convert))
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(Pre_processLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                    .addComponent(contrast)
                    .addComponent(saveAsTiff)
                    .addComponent(deleteSlice))
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(Pre_processLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                    .addComponent(rotate)
                    .addComponent(saveImgSeq)
                    .addComponent(crop))
                .addGap(6, 6, 6)
                .addGroup(Pre_processLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                    .addComponent(importImgSeq)
                    .addComponent(enlarge5)
                    .addComponent(shrink5))
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(Pre_processLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                    .addComponent(grid96)
                    .addComponent(grid384))
                .addGap(42, 42, 42)
                .addGroup(Pre_processLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
                    .addComponent(measure, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(getMeseasurements, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(175, 175, 175))
        )

        layout = GroupLayout(self.getContentPane())
        self.getContentPane().setLayout(layout)
        layout.setHorizontalGroup(
            layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(Pre_process, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addContainerGap(32, Short.MAX_VALUE))
        )
        layout.setVerticalGroup(
            layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap(24, Short.MAX_VALUE)
                .addComponent(Pre_process, GroupLayout.PREFERRED_SIZE, 485, GroupLayout.PREFERRED_SIZE))
        )


        # start it up
        self.setTitle("Command Center")
        self.setSize(520,400)
        self.setLocationRelativeTo(None)
        self.setVisible(True)

    # button actions

    def onSwimStack(self, event):
        IJ.run("Swim measure stack v2 ")

    def onAnalyze(self, event):
        IJ.run("Profiles ")

    def onGetMeasurementsStack(self, event):
        IJ.run("Get plate measurements stack ")

    def onRotate(self, event):
        IJ.run("Rotate... ")

    def onScale(self, event):
        IJ.run("SetScale gelDoc ")

    def onNonSwimmers(self, event):
        IJ.run("Mark nonSwimmers ")

    def onCrop(self, event):
        IJ.run("Crop")

    def onConvert(self, event):
        IJ.run("8-bit")

    def onContrast(self, event):
        IJ.run("Enhance Contrast", "saturated=0.35")

    def onSaveAsTiff(self, event):
        image = WindowManager.getCurrentImage()
        IJ.saveAsTiff(image, "")

    def onSaveImgSeq(self, event):
        IJ.run("Image Sequence... ")

    def onGetMeasurement(self, event):
        IJ.run("Get plate measurements ")

    def onDeleteSlice(self, event):
        IJ.run("Delete Slice")

    def onImportImgSeq(self, event):
        IJ.run("Image Sequence...")

    def onOpenImg(self,event):
        IJ.open()

    def onEnlarge5(self, event):
         IJ.run("Enlarge 5px ")

    def onShrink5(self,event):
        IJ.run("Shrink 5px ")

    def onGrid96(self,event):
        IJ.run("ROI Grid ", "rows=8 columns=12")

    def onGrid384(self,event):
        IJ.run("ROI Grid ", "rows=16 columns=24")


if __name__ == '__main__':
    CommandCenter()