# Phenotype Analysis in Fiji (PAF)

## Overview
This is a module and collection of scripts written in Jython, designed to be run in Fiji (ImageJ), that semi-automate the process of measuring phenotypes from images.  As of now protease, rhamnolipids, hemolysis, siderophore, HCN production and swimming motility (both static and time-course) are supported for 96 and 384-well formats.

This is research code written specifically for a single project and is likely not broadly useful.  However, it has been made available for openess and reproducability and for those doing similar work who may wish to use this code as an example.

## Installation
* Download and install [Fiji](http://fiji.sc/Fiji) for whatever system you are using.
* Clone this repository into Fiji's plugins directory.
```
git clone https://mworkentine@bitbucket.org/mworkentine/phenotype_analysis_fiji.git
```
* Start Fiji and under the 'Plugins' menu you should find 'phenotype_analysis_fiji'.  In there click on 'Command Center' to run the analysis.

## Dependencies

* Fiji - tested with ImageJ 2.0.0-rc/1.49d; Java 1.6.0_24 [64-bit].  The code was written with the ImageJ 1 API but has been modified to work with ImageJ2.


## Credits