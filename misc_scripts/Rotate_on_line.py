# Line rotator
'''Rotates an image base on the angle of a user-drawn line'''

import sys

def getAngle(imp, line):
	'''given a line roi returns the angle'''

	measurements = Analyzer.RECT
	rt = ResultsTable()
	An = Analyzer(imp, measurements, rt)
	An.measure()
	return rt.getColumn(rt.getColumnIndex("Angle"))


def main():
	imp = IJ.getImage()
	roi = imp.getRoi()
	if not roi:
		IJ.error("Please make line selection")
	if roi.getType() != Roi.LINE:
	    IJ.error("Please use line roi!")
	    sys.exit()
    	
   	angle = getAngle(imp, roi)[0]
     	
   	rotate_arg = "angle=%s grid=10 interpolation=Bilinear" % angle
   	IJ.run(imp, "Rotate... ", rotate_arg)

if __name__ == '__main__':
	main()