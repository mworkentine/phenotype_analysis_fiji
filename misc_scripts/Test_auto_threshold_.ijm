
impID = getImageID();

setBatchMode(true);

for (i=1; i<=60; i+=2){
	selectImage(impID);
	run("Duplicate...", "title=&i");
	run("Auto Local Threshold", "method=Otsu radius=&i parameter_1=0 parameter_2=0 white");	
}

run("Images to Stack", "name=AutoThreshold_Test title=[] use");

setBatchMode(false);