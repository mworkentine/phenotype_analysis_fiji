import sys
IJ.run("Set Scale...", "distance=0 known=0 pixel=1 unit=pixel")
imp = IJ.getImage()
roi = imp.getRoi()
if not roi:
    IJ.error("Please make line selection")
if roi.getType() != Roi.LINE:
    IJ.error("Please use line roi!")
    sys.exit()

length = roi.getLength()
scale_arg = "distance=%s known=20 pixel=1 unit=mm" % length
print scale_arg

IJ.run("Set Scale...", scale_arg)