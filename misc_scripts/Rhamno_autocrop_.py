# autocrop 
# needs to have large contrast difference between foreground and 
# background

import ij.gui.Wand
from ij.plugin.filter import Binary
from phenotype import phenotype as ph
from os import listdir

def preProcess(image):
    IJ.run(image,"Auto Threshold", "method=Mean white")
    for i in range(1,20):
        IJ.run(image,"Erode", "")

def findCenter(ip):
    '''Finds the center area of the image for autocropping'''

    w = Wand(ip)

    height = ip.getHeight()
    width = ip.getWidth()
    center = (width/2, height/2)

    w.autoOutline(center[0], center[1], 1, 255, w.EIGHT_CONNECTED)

    return PolygonRoi(w.xpoints, w.ypoints, w.npoints, Roi.TRACED_ROI)

def autoCrop(image):
    '''tries to autocrop the image to the area with colonies'''
    
    IJ.run(image, "8-bit", "")
    image2 = Duplicator().run(image)

    preProcess(image2)
    roi = findCenter(image2.getProcessor())
        
    image.setRoi(roi)

    if roi.getNCoordinates() != 0:
        IJ.run(image,"To Bounding Box", "")
        IJ.run(image, "Crop", "")
        IJ.run(image, "Enhance Contrast", "saturated=0.35")


def main():

    image = WindowManager.getCurrentImage()
    autoCrop(image)




if __name__ == '__main__':
    main()