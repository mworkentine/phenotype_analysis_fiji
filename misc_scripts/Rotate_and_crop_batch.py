###################
# Rotate and crop #
###################

'''Rotate and crop antibiotic images in batch, need to specify rotation and 
crop bounding box manually for each set of images'''

from ij.io import FileSaver
from ij.io import DirectoryChooser
import sys
from ij.plugin.filter import Rotator

def getDirectory(folder_type):
    """ Get the directory for image processing"""
    dialoge_title = "Choose an "+ folder_type+" folder" 
    dc = DirectoryChooser(dialoge_title)
    folder = dc.getDirectory()

    if folder is None:
        IJ.error("No directory chosen")
        sys.exit()
    
    return folder

def process(image):
    ''' rotates and crops the image '''

    # rotate
    rotater = Rotator()
    rotater.setup("angle=-0.50 grid=10 interpolation=Bilinear", image)
    rotater.run()
    # set crop rectangle
    ij.plugin.Resizer("crop")

def main():
    
    input_folder = getDirectory("input")
    output_folder = getDirectory("output")

    file_list = listdir(input_folder)
    if not file_list:
        IJ.error("No files in directory.  Exiting...")
        sys.exit()

    for imageFile in file_list:
        in_path = input_folder + imageFile
        out_path = output_folder + imageFile

        image = IJ.openImage(in_path)
        process(image)
        fs = FileSaver(image)
        fs.saveAsTiff(out_path)
        image.close()   

if __name__ == '__main__':
    main()