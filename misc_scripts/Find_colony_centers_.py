# find colony center's
from ij.plugin.filter import ParticleAnalyzer as PA


def rhamnolipid_process(image):
    """ Function that takes a reference to ImagePlus image and performs
    pre-processing/filtering prior to peak detection, optimized for rhamnolipid
    image types.
    """
    
    # bandpass filter evens out the illumination
    IJ.run(image, "Bandpass Filter...", \
       "filter_large=500 filter_small=0 suppress=None tolerance=5 autoscale saturate")

    # median filter removes artifacts, smooths out the image
    IJ.run(image, "Median...", "radius=10")
    IJ.run(image,"Subtract Background...", "rolling=50 light sliding")

	
def getColonyCenters(image, phenotype):
	'''Takes a cropped and processed image and finds all the colony centers.
	As of now this will be done for rhamnolipid images but will be adapted for 
	other ones in the future'''

	# threshold
	# this is the part that will be different for each image
	image2 = Duplicator().run(image)
	if phenotype == "Rhamnolipid":
		IJ.run(image2, "Auto Threshold", "method=MaxEntropy white")
	
	# use particle analyzer
	MAXSIZE = 100000
	MINSIZE = 1
	MINCIRC = 0.3
	MAXCIRC = 1.0
	
	options = PA.SHOW_NONE 
	measurements = PA.CENTER_OF_MASS
	
	rt = ResultsTable()
	
	p = PA(options, measurements, rt, MINSIZE, MAXSIZE, MINCIRC, MAXCIRC)
	
	p.setHideOutputImage(True)
	p.analyze(image2)

	# get the results from the results table
	x_coord = rt.getColumn(rt.getColumnIndex("XM"))
	y_coord = rt.getColumn(rt.getColumnIndex("YM"))
	colony_coords = [(x_coord[i],y_coord[i]) for i in range(0,len(x_coord))]

	image2.close()
	
	return colony_coords



image = WindowManager.getCurrentImage()

colony_coords = getAllColonies(image, "Rhamnolipid")

print colony_coords


