inputDir = getDirectory("Choose Input Directory");
outputDir = getDirectory("Choose Output Directory");

fileList = getFileList(inputDir)

setBatchMode(true)
for (i=0; i<fileList.length; i++) {
    showProgress(i+1, fileList.length);
    open(inputDir+fileList[i]);

    run("Rotate... ", "angle=-0.50 grid=10 interpolation=Bilinear");
    makeRectangle(238, 558, 1540, 1022);
    run("Crop");
    run("Enhance Contrast", "saturated=0.35");
    run("8-bit");

    saveAs("TIFF", outputDir+fileList[i]);
    close();

}

setBatchMode(false)

showMessage("Completed Analysis")

