
def next_left_peak(peaks, mid):
    """Find the next smallest peak from mid in a list"""
    
    leftPeaks = [x for x in peaks if x < mid]
    if len(leftPeaks) == 0:
        return 0
    else:
        return max(leftPeaks)
  

def next_right_peak(peaks, mid):
    """Find the next largest peak from mid in a list"""

    rightPeaks = [x for x in peaks if x > mid]
    if len(rightPeaks) == 0:
        return 0
    else:
        return min(rightPeaks)

def gen_pairs(peaks, mid):
    """Finds the next middle pair of peaks from a list of peaks"""
    
    while len(peaks) >= 2:
        left = next_left_peak(peaks, mid)
        right = next_right_peak(peaks, mid)
        peaks.remove(left)
        peaks.remove(right)
        yield (left, right)


def get_pairs(peaks, mid):
    """Function that uses the pairs generator, gen_pairs to return
    the colony and zone peaks"""
    
    pairs =  gen_pairs(peaks, mid)

    try:
        colonyPeaks = pairs.next()
    except StopIteration:
        colonyPeaks = None

    try:
        zonePeaks = pairs.next()
    except StopIteration:
        if len(peaks) == 1:
            zonePeaks = peaks
        else:
            zonePeaks = None

    return colonyPeaks, zonePeaks

def calc_diameter(colonyPeaks, zonePeaks):
    """Function that takes colony peaks and zone peaks and calculates
    the zone diameter"""
    
    # case 1: colony and zone
    if colonyPeaks and zonePeaks:
        if len(zonePeaks) == 1:
            leftval = abs(zonePeaks[0] - colonyPeaks[0])
            rightval = abs(zonePeaks[0] - colonyPeaks[1])
            diameter = min(leftval, rightval)
        else:
            diameter = max(zonePeaks) - min(zonePeaks)

    # case 2: colony only
    if colonyPeaks and not zonePeaks:
        diameter = 0

    return diameter

peaks = [37, 74, 111, 151]
peaks2 = [96, 65]
peaks3 = [32, 41, 103, 82, 57]
peaks4 = [104, 55, 47, 120, 71]
peaks5 = [57, 101, 1]
peaks6 = [118, 55, 99, 68]

colonyPeaks, zonePeaks = get_pairs(peaks6,80)

print calc_diameter(colonyPeaks, zonePeaks)
