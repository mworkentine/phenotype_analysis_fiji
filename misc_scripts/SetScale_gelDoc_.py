def setScale(image):
    """ Function to set the scale on images acquired with the AlphaInnotech
    HD2 gel doc """

    cal = image.getCalibration()
    cal.pixelWidth = 0.0737687
    cal.pixelHeight = 0.0737687
    cal.pixelDepth = 0.0737687
    cal.setUnit("mm")

    image.setCalibration(cal)
    image.repaintWindow()

setScale(IJ.getImage())