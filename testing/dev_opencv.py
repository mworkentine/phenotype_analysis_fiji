from com.googlecode.javacv.cpp.opencv_core import *
from com.googlecode.javacv.cpp.opencv_imgproc import *
from java.awt.image import BufferedImage

img = WindowManager.getCurrentImage()
ip = img.getProcessor()
bi = ip.getBufferedImage()

iplsrc = IplImage.createFrom(bi)

edge = cvCreateImage(cvSize(iplsrc.width(), iplsrc.height()), IPL_DEPTH_8U, 1)


medianBlur(iplsrc, edge, 111)

out_buff_image = edge.getBufferedImage()

newImg = ImagePlus("output edges", out_buff_image)
newImg.show()