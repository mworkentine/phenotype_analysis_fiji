def rhamnolipid_process(image, radius, p1):
    """ Function that takes a reference to ImagePlus image and performs
    pre-processing/filtering prior to peak detection, optimized for rhamnolipid"""
    threshold = "Bernsen"
    IJ.run(image,"Subtract Background...", "rolling=100 light sliding")
    thresholdParam = "method=%s radius=%s parameter_1=%s parameter_2=0" % (threshold, radius, p1)
    IJ.run(image,"Auto Local Threshold", thresholdParam)
    IJ.run(image,"Close-", "")
    IJ.run(image, "Fill Holes", "")
    IJ.run(image, "Find Edges", "")
    #IJ.run(image, "Dilate", "")


image = WindowManager.getCurrentImage()
ip = image.getProcessor()

stack = ImageStack(image.width, image.height)
sliceNum = 1
radius = 10

for p1 in range(20,21):
	image2 = Duplicator().run(image)
	rhamnolipid_process(image2, radius, p1)
	newIp = ip.duplicate()
	mask = image2.getProcessor().getMask()
	mask.invert()
	newIp.fill(mask)
	stack.addSlice(newIp)
	label = "radius: %s | p1: %s" % (radius, p1)
	stack.setSliceLabel(label, sliceNum)
	sliceNum += 1

finalImage = ImagePlus("Thresholded Stack", stack)
finalImage.show()