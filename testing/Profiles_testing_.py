from org.rosuda.REngine.Rserve import RConnection
from ij.gui import ProfilePlot
from ij.gui import Roi, Overlay
import java.awt


def get_peaks(profile, rconnection):
    """Function that takes a pixel profile and and an open R connection (via Rserve)
    and returns the min and max peak in the profile"""

    c.assign("prof", profile)
    peaks = c.eval("SpectrumSearch(prof, sigma=2, threshold=5)").asList()
    peaksX =  peaks[0].asIntegers()
    return peaksX

def calc_zone(peaks, profile):
    """ Function that takes a list of peaks and corresponding intensities and
    calculates the presence of a colony and the diameter of the zone.  Returns
    first a boolean indicating the presence/absence of a colony as well as the 
    colony centerthen returns the diameter.  A colony value of True with a diameter 
    of 0 means no zone.
    """

    # right off the bat spits a false for colony if there is 1 or less peaks
    if len(peaks) <= 1: 
        return False, 0, 0

    # find the peaks within the middle of the roi
    size = len(profile)
    colonyBorders = (size/4, size-size/4) #controls where the 'middle' is
    colonyPeaks = filter(lambda x: colonyBorders[0] < x < colonyBorders[1], peaks)

    print "Colony Borders: ", colonyBorders
    # if this is empty (i.e. no colony peaks) get out
    if len(colonyPeaks) <= 1:
        return False, 0, 0

    # if the colony peaks are below an intensity of 10, this is noise
    if profile[colonyPeaks[0]] < 10:
        return False, 0, 0

 
    assert len(colonyPeaks) >= 2, "Less than two colony peaks.  Shouldn't be here!!"

    print "Colony peaks prior to sorting", colonyPeaks
    # figure out what to do if we have more than two peaks in the center
    if len(colonyPeaks) == 4:
        # probably a good colony with zone, sort and take middle two
        print "4 peaks found...sorting"
        colonyPeaks.sort()
        colonyPeaksTop = colonyPeaks[1:3]
    else:
        # assume top two are most intense and are colony peaks
        print "Taking top two peaks"
        colonyPeaksTop = colonyPeaks[0:2]
           
    
    ### find the smallest distance between the nearest zone peak and the colony peaks

    assert len(colonyPeaksTop) == 2, "Should only have two peaks here!"
    print colonyPeaksTop
    leftColony = min(colonyPeaksTop)
    rightColony = max(colonyPeaksTop)
    colonyCenter = (abs(leftColony-rightColony) / 2) + leftColony
   
    assert colonyCenter > 0, "Whaaat? Colony center is less than zero, whoops!"

    # get the left zone size
    leftPeaks = [x for x in peaks if x < leftColony]
    if len(leftPeaks) == 0:
        leftZone = 0
    else:
        leftZone = colonyCenter - max(leftPeaks)

    # get the right zone size
    rightPeaks = [x for x in peaks if x > rightColony]
    if len(rightPeaks) == 0:
        rightZone = 0
    else:
        rightZone = min(rightPeaks) - colonyCenter

    
    if leftZone and rightZone != 0:
        radius = min(leftZone, rightZone)
        return True, colonyCenter, radius*2
    elif leftZone == 0: return True, colonyCenter, rightZone*2
    elif rightZone == 0: return True, colonyCenter, leftZone*2


c = RConnection()
c.eval("library(Peaks)")
c.eval("library.dynam('Peaks', 'Peaks', lib.loc=NULL)")

imp = IJ.getImage()
roi = imp.getRoi()
init_profile = roi.getPixels()
init_peaks = get_peaks(init_profile, c)

# scale the profile to the smallest and largest peaks
# 
if init_peaks is not None:
    leftSide = int(min(init_peaks) - min(init_peaks) * 0.2)
    rightSide = int(max(init_peaks) + max(init_peaks) * 0.2)
    profile = init_profile[leftSide:rightSide+1]
    peaks = get_peaks(profile, c)
else:
    print "Umm...got not peaks, not sure how to handle this..."
    sys.exit()

#pp = ProfilePlot(imp)
#pp.createWindow()

intensity = [profile[x] for x in peaks]

#convert to floats
peaksFloat = [float(x) for x in peaks]
intensityFloat = [float(x) for x in intensity]

for i in range(0,len(intensity)):
    print str(peaks[i]) + "  " + str(intensity[i])


distance = range(1,len(profile)+1)
plot = Plot("My Profile", "distance", "profile", distance, profile)
plot_window = plot.show()
plot_window.setColor(java.awt.Color.RED) 
plot_window.addPoints(peaksFloat, intensityFloat, Plot.CIRCLE)
plot_window.draw()

colony, colonyCenter, diameter = calc_zone(peaks, profile)
print colony, diameter



c.close()