from org.rosuda.REngine.Rserve import RConnection
from ij.gui import ProfilePlot
from ij.gui import Roi, Overlay
import java.awt


def get_peaks(profile, rconnection):
    """Function that takes a pixel profile and and an open R connection (via Rserve)
    and returns the min and max peak in the profile"""

    c.assign("prof", profile)
    peaks = c.eval("SpectrumSearch(prof, sigma=2, threshold=5)").asList()
    peaksX =  peaks[0].asIntegers()
    return peaksX

def next_left_peak(peaks, mid):
    """Find the next smallest peak from mid in a list"""
    
    leftPeaks = [x for x in peaks if x < mid]
    if len(leftPeaks) == 0:
        return 0
    else:
        return max(leftPeaks)
  

def next_right_peak(peaks, mid):
    """Find the next largest peak from mid in a list"""

    rightPeaks = [x for x in peaks if x > mid]
    if len(rightPeaks) == 0:
        return 0
    else:
        return min(rightPeaks)

def gen_pairs(peaks, mid):
    """Finds the next middle pair of peaks from a list of peaks"""
    
    while len(peaks) >= 2:
        left = next_left_peak(peaks, mid)
        right = next_right_peak(peaks, mid)
        peaks.remove(left)
        peaks.remove(right)
        yield (left, right)

def get_pairs(peaks, mid):
    """Function that uses the pairs generator, gen_pairs to return
    the colony and zone peaks"""
    
    pairs =  gen_pairs(peaks, mid)

    try:
        colonyPeaks = pairs.next()
    except StopIteration:
        colonyPeaks = None

    try:
        zonePeaks = pairs.next()
    except StopIteration:
        if len(peaks) == 1:
            zonePeaks = peaks
        else:
            zonePeaks = None

    return colonyPeaks, zonePeaks

def calc_diameter(colonyPeaks, zonePeaks):
    """Function that takes colony peaks and zone peaks and calculates
    the zone diameter"""
    
    # case 1: colony and zone
    if colonyPeaks and zonePeaks:
        if len(zonePeaks) == 1:
            leftval = abs(zonePeaks[0] - colonyPeaks[0])
            rightval = abs(zonePeaks[0] - colonyPeaks[1])
            diameter = min(leftval, rightval)
        else:
            diameter = max(zonePeaks) - min(zonePeaks)

    # case 2: colony only
    if colonyPeaks and not zonePeaks:
        diameter = 0

    return diameter


c = RConnection()
c.eval("library(Peaks)")
c.eval("library.dynam('Peaks', 'Peaks', lib.loc=NULL)")

imp = IJ.getImage()
roi = imp.getRoi()
profile = roi.getPixels()
peaks = get_peaks(profile, c)
print peaks

#colonyPeaks, zonePeaks = get_pairs(peaks,80)

#diameter = calc_diameter(colonyPeaks, zonePeaks)

#print diameter

#pp = ProfilePlot(imp)
#pp.createWindow()

intensity = [profile[x] for x in peaks]

#convert to floats
peaksFloat = [float(x) for x in peaks]
intensityFloat = [float(x) for x in intensity]

print peaks

distance = range(1,len(profile)+1)
plot = Plot("My Profile", "distance", "profile", distance, profile)
plot_window = plot.show()
plot_window.setColor(java.awt.Color.RED) 
plot_window.addPoints(peaksFloat, intensityFloat, Plot.CIRCLE)
plot_window.draw()

#colony, colonyCenter, diameter = calc_zone(peaks, profile)
#print colony, diameter



c.close()