
def threshold(ip):
	ip.setThreshold(147, 147, ImageProcessor.NO_LUT_UPDATE)
	# Call the Thresholder to convert the image to a mask
	IJ.run(imp, "Convert to Mask", "")

def findCenter(ip):
    '''Finds the center area of the image for autocropping'''

    w = Wand(ip)

    height = ip.getHeight()
    width = ip.getWidth()
    center = (width/2, height/2)

    w.autoOutline(center[0], center[1], 1, 255, w.EIGHT_CONNECTED)

    return PolygonRoi(w.xpoints, w.ypoints, w.npoints, Roi.TRACED_ROI)


image1 = WindowManager.getCurrentImage()
roi = findCenter(image1.getProcessor())
image1.setRoi(roi)

