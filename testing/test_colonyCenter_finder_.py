from ij.plugin.filter import ParticleAnalyzer as PA

def getColonyCenters(image, phenotype):
    """Takes a cropped and processed image and finds all the colony centers.
    As of now this will be done for rhamnolipid images but will be adapted for 
    other ones in the future"""

    # threshold
    # this is the part that will be different for each image
    image2 = Duplicator().run(image)
    if phenotype == "Rhamnolipid":
        IJ.run(image2, "Auto Threshold", "method=MaxEntropy")
    else:
        IJ.error("Not yet implemented for this phenotype")
        sys.exit()

	# reset the scale so the results table will return pixel units
    IJ.run(image2,"Set Scale...", "distance=0 known=0 pixel=1 unit=pixel")

    for i in range(1,5):
        IJ.run(image2,"Erode", "")
    
    #image2.show()

    # use particle analyzer
    MAXSIZE = 100000
    MINSIZE = 1
    MINCIRC = 0.3
    MAXCIRC = 1.0
    
    options = PA.SHOW_NONE 
    measurements = PA.CENTER_OF_MASS
    
    rt = ResultsTable()
    
    p = PA(options, measurements, rt, MINSIZE, MAXSIZE, MINCIRC, MAXCIRC)
    
    p.setHideOutputImage(True)
    p.analyze(image2)

    # get the results from the results table
    x_coord = rt.getColumn(rt.getColumnIndex("XM"))
    y_coord = rt.getColumn(rt.getColumnIndex("YM"))
    colony_coords = [(x_coord[i],y_coord[i]) for i in range(0,len(x_coord))]

    
    return colony_coords



image = WindowManager.getCurrentImage()
colonies = getColonyCenters(image, "Rhamnolipid")

if RoiManager.getInstance() is None:
	manager = RoiManager()
else:
    manager = RoiManager.getInstance()

numRow = 8
numCol = 12
ip = image.getProcessor()
imgHeight = ip.getHeight()
imgWidth = ip.getWidth()
height = imgHeight / numRow
width = imgWidth / numCol

for colony in colonies:
    start_x = colony[0] - width/2
    start_y = colony[1] - height/2
    end_x = colony[0] + width/2
    end_y = colony[1] + height/2

    roi = Line(start_x, start_y, end_x, end_y)
    manager.addRoi(roi)

