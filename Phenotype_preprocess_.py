# Phenotype preprocessor script
# =============================

"""This script runs a series of pre-processing steps on a phenotype image
regardless of the phenotype"""


from phenotype import phenotype as ph
from os import listdir


def main():

    # get the input and output directorys
    input_folder = ph.get_directory("input")
    output_folder = ph.get_directory("output")

    # get file list from input folder
    file_list = listdir(input_folder)
    if not file_list:
        IJ.error("No files in directory.  Exiting...")
        sys.exit()

    ## --- for debugging in ImageJ
    # image = WindowManager.getCurrentImage()
    # image2 = Duplicator().run(image)

    for imageFile in file_list:

        file_path = input_folder + imageFile
        image = IJ.openImage(file_path)

        ph.phenotype_preprocess(image)

        out_path = output_folder + imageFile
        FileSaver(image).saveAsTiff(out_path)

        image.changes = False
        image.close()

    IJ.showMessage("All done!") 



if __name__ == '__main__':
    main()