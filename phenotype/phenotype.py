""" A python module to hold the functions required for automated phenotype
analysis in Fiji (ImageJ)
"""

# ========================================================================

from ij.plugin.filter import ParticleAnalyzer as PA
from ij.plugin.filter import Filters, Binary

from mpicbg.ij.clahe import Flat, FastFlat

from ij.io import DirectoryChooser

from ij.plugin.frame import RoiManager
from ij.plugin import Duplicator
from ij.measure import ResultsTable
from ij.measure import Measurements
from ij.process import ImageStatistics

from ij.gui import GenericDialog
from ij.gui import ProfilePlot
from ij.gui import OvalRoi
from ij.gui import Line
from ij.gui import Roi

from ij import WindowManager
from ij import ImagePlus
from ij import IJ

import java.awt
import string
import math
import csv
import sys
import re



# =========================================================================


class PeakProfile(object):
    """Class to handle peak finding and colony and zone identification in peaks
    from phenotyping plates. """

    def __init__(self, profile):
        self.profile = profile
        self.peaks = self.find_peaks()

    def find_peaks(self):
        """ Function to peaks in a binary image.  Scans along vector and returns of 
        any peaks above zero"""

        x = self.profile
        peaks = []
        peak = []
        
        for i in range(0, len(x)):

            if i not in (0,len(x)-1):

                # no peak
                if x[i] == 0 and x[i-1] == 0 and x[i+1] == 0:
                    continue
        
                # start
                if x[i] > 0 and x[i-1] == 0 and x[i+1] > 0:
                    peak.append(i)

                # middle
                if x[i] > 0 and x[i-1] > 0 and x[i+1] > 0:
                    peak.append(i)
      
                # end
                if x[i] > 0 and x[i-1] > 0 and x[i+1] == 0:
                    peak.append(i)
                    if len(peak) > 1:
                        mean_peak = float(sum(peak))/len(peak)
                        peaks.append(int(mean_peak))
                    peak = []

            else:
                continue

        return peaks

    def next_left_peak(self, peaks, mid):
        """Find the next smallest peak from mid in a list"""
        
        leftPeaks = [x for x in peaks if x < mid]
        if len(leftPeaks) == 0:
            return None
        else:
            return max(leftPeaks)
  
    def next_right_peak(self, peaks, mid):
        """Find the next largest peak from mid in a list"""

        rightPeaks = [x for x in peaks if x > mid]
        if len(rightPeaks) == 0:
            return None
        else:
            return min(rightPeaks)


    def gen_pairs(self, peaks, mid):
        """Finds the next middle pair of peaks from a list of peaks"""
        
        while len(peaks) >= 2:
            left = self.next_left_peak(peaks, mid)
            right = self.next_right_peak(peaks, mid)
            peaks.remove(left)
            peaks.remove(right)
            yield (left, right)


    def get_pairs(self, mid):
        """Function that uses the pairs generator, gen_pairs to return
        the colony and zone peaks"""
        
        peaks = list(self.peaks)
        pairs =  self.gen_pairs(peaks, mid)

        try:
            colonyPeaks = pairs.next()
        except (StopIteration, ValueError):
            colonyPeaks = None

        try:
            zonePeaks = pairs.next()
        except (StopIteration, ValueError):
            if len(peaks) == 1:
                zonePeaks = peaks
            else:
                zonePeaks = None

        return colonyPeaks, zonePeaks


    def plot(self):
        """ Function to plot out the profile, used for debugging mostly"""

        profile = self.profile
        peaks = self.peaks
        intensity = [profile[x] for x in peaks]

        #convert to floats
        peaksFloat = [float(x) for x in peaks]
        intensityFloat = [float(x) for x in intensity]

        distance = range(1,len(profile)+1)
        plot = Plot("Profile", "distance", "profile", distance, profile)
        plot_window = plot.show()
        plot_window.setColor(java.awt.Color.RED) 
        plot_window.addPoints(peaksFloat, intensityFloat, Plot.CIRCLE)
        plot_window.draw()

#===========================================================================

def find_zone_coords(colonyPeaks, x, y, diameter):
    """ Function that takes a tuple with location of colony peaks, the upper
    left coordinates of the current spot and the zone radius.  Returns
    the upper left coordinates of the zone relative the to colony center
    """
    
    radius = diameter / 2
    
    # calculate the colony center relative to the current spot
    colonyWidth = abs(colonyPeaks[1]- colonyPeaks[0])
    colonyCenter = min(colonyPeaks) + colonyWidth/2

    # because the colony center is calculated relative to the diaganol
    # of the spot, have to do a little trig to get proper values
    col_x = math.sin(math.radians(45)) * colonyCenter
    col_y = math.sqrt(colonyCenter**2 - col_x**2)
    zone_x = int(x + (col_x - radius))
    zone_y = int(y + (col_y - radius))
    
    return zone_x, zone_y


def median(l):
    """ Function that takes a list and returns the median value of that list
    """

    srtd = sorted(l) # returns a sorted copy
    mid = len(l)/2   # remember that integer division truncates

    if len(l) % 2 == 0:  # take the avg of middle two
        return (srtd[mid-1] + srtd[mid]) / 2.0
    else:
        return srtd[mid]


def calc_diameter(colonyPeaks, zonePeaks):
    """Function that takes colony peaks and zone peaks and calculates
    the zone diameter"""
    
    colonyWidth = abs(colonyPeaks[1]- colonyPeaks[0])

    if len(zonePeaks) == 1:
        leftval = abs(zonePeaks[0] - colonyPeaks[0])
        rightval = abs(zonePeaks[0] - colonyPeaks[1])
        diameter = 2*(min(leftval, rightval) + 0.5*(colonyWidth))
    else:
        diameter = max(zonePeaks) - min(zonePeaks)

    return int(diameter)


def get_peaks(profile, rcon):
    """Function that takes a pixel profile and and an open R connection 
    (via Rserve) and returns the min and max peak in the profile"""

    rcon.eval("library(Peaks)")
    rcon.eval("library.dynam('Peaks', 'Peaks', lib.loc=NULL)")
    rcon.assign("prof", profile)
    peaks = rcon.eval("SpectrumSearch(prof, sigma=2, threshold=5, markov = FALSE)").asList()
    peaksX =  peaks[0].asIntegers()
    smoothProf = peaks[1].asDoubles()
    return peaksX, smoothProf


def get_directory(folder_type):
    """Get the directory for image processing"""
    
    dialoge_title = "Choose an "+ folder_type+" folder" 
    dc = DirectoryChooser(dialoge_title)
    folder = dc.getDirectory()

    if folder is None:
        IJ.error("No directory chosen")
        sys.exit()
    
    return folder


def get_options(stamp_default, sample, media, replicate ):
    """ This function gets the plate type from the user and returns the 
    correct labels as well as the number of rows and columns."""

    stamps = ("1","2","3","4","96-well", "384-combined")
    gd = GenericDialog("Plate Options")
    
    # for the stamp number - should be already correct based on the file name
    gd.addRadioButtonGroup("Select stamp number", stamps, 2, 3, \
    	stamps[stamp_default])
    
	# for the sample, media and replicate
    # should be filled from the image name but if not then can be done here
    gd.addStringField("Sample", sample)
    gd.addStringField("Media", media)
    gd.addStringField("Replicate", replicate)

    # bring it up
    gd.hideCancelButton()
    gd.showDialog()

    # get the values
    stamp = gd.getNextRadioButton()
    sample = gd.getNextString()
    media = gd.getNextString()
    replicate = gd.getNextString()

    # convert media type to full name from abreviation
    mediaConversions = {"HEM": "Hemolysis",
    					"PRO": "Protease",
    					"CAS": "Siderophore",
    					"RHA": "Rhamnolipid"}
    
    if mediaConversions.get(media) is not None:
    	media = mediaConversions.get(media)

    # return the correct stamp labels based on stamp type
    if stamp == "96-well":
        letters = string.ascii_uppercase[0:8]
        numbers = map(str, range(1,12 + 1))
        spotLabels = [(i + j) for i in letters for j in numbers]
        numRow = 8
        numCol = 12
    elif stamp == "384-combined":
        spotLabels = get_plate_labels_full()
        numRow = 16
        numCol = 24
    else:
        spotLabels = get_plate_labels(int(stamp))
        numRow = 8
        numCol = 12
    
    return spotLabels, numRow, numCol, sample, media, replicate


def order_set(seq, idfun=None): 
    """ Function to create unique set that is order preserving. Takes a 
    sequence and returns an ordered set of the seqeunce.

    Taken from here: http://www.peterbe.com/plog/uniqifiers-benchmark
    """

    if idfun is None:
        def idfun(x): return x
    
    seen = {}
    result = []
    
    for item in seq:
        marker = idfun(item)
        if marker in seen: continue
        seen[marker] = 1
        result.append(item)
    
    return result


def make_grid(width, height, numRow, numCol):
    """ Makes the  grid based on the given number of rows and columns. 
    Returns a dictionary with well labels as keys and roi coordinates 
    as values.  """

    """ unit tested """

    letters = string.ascii_uppercase[0:numRow]
    numbers = map(str, range(1,numCol + 1))

    grid = {}
    for i in range(0, numRow):
        for j in range(0, numCol):
            
            x = j * width
            y = i * height
            currentWell = letters[i] + numbers[j]
            grid[currentWell] = (x, y, width, height)

    return grid
       

def make_grid_location(image, spotLabels, numRow, numCol, modifier=0):
    """ Makes the  grid and returns two dictionarys of well labels as keys
    and grid coordinates.  First is for the rows and second is for columns.
    Different from make_grid in that it returns the coordinates for each well 
    rather than the width and height. 
    """

    """ unit tested """
    
    # get image height and width
    ip = image.getProcessor()
    imgHeight = ip.getHeight()
    imgWidth = ip.getWidth()
       
    # set paramters
    height = (imgHeight / numRow)
    width  = (imgWidth / numCol)

    # modifier for well size, reduces detection of false zones outside wells
    mH = modifier
    mW = modifier
    
    # make the labels based on number of columns and rows
    regex = re.compile("([A-P])(\d*)")
    letters = list()
    numbers = list()
    for spot in spotLabels:
        label = regex.search(spot)
        letters.append(label.groups()[0])
        numbers.append(label.groups()[1])

    letters = list(order_set(letters))
    numbers = list(order_set(numbers))

    # row coordinates
    rowCoords = {}
    for i in range(0, numRow):
        y1 = (i * height)
        y2 = y1 + (height - 1)
        rowCoords[letters[i]] = (y1+mH,y2-mH)

    # column coordinatesm
    colCoords = {}
    for j in range(0, numCol):
        x1 = (j * width)
        x2 = x1 + (width - 1)
        colCoords[numbers[j]] = (x1+mW,x2-mW) 

    return rowCoords, colCoords


def get_location(pointCoords, rowCoords, colCoords):
    """ Get the grid square location of a point (row, col) in image based on 
        the coordinates in gridRows and gridCols which are dictionaries
    """
    
    """ Unit tested """

    # get row value
    rowValue = None
    for key in rowCoords:
        if rowCoords[key][0] <= pointCoords[0] <= rowCoords[key][1]:
            rowValue = key
        
    # get col value
    colValue = None
    for key in colCoords:
        if colCoords[key][0] <= pointCoords[1] <= colCoords[key][1]:
            colValue = key

    if rowValue and colValue:
        return rowValue + colValue
    else:
        return None


def get_roi_center(roiBounds):
    """Get the center of roi based on given bounds.  Takes an roiBounds object
    and returns the center coordinates
    """

    x = roiBounds.x
    y = roiBounds.y
    height = roiBounds.height
    width = roiBounds.width

    center_x = x + (width / 2)
    center_y = y + (height / 2)

    return center_y, center_x
                     
    
def get_plate_labels(stamp):
    """ This function gets the correct labels for the current image based on 
    the plate stamping. Takes the stamp number and retuns a list of well labels
    based on the which stamp was provided.
    """
    
    indexRanges = {1:range(0,337, 48), 2:range(1,338, 48), \
    3:range(25,362, 48), 4:range(24,361, 48)}
    
    letters = string.ascii_uppercase[0:16]
    numbers = map(str, range(1,24 + 1))
    full_labels = [(i + j) for i in letters for j in numbers]


    index = [(full_labels[j]) for i in indexRanges[stamp] \
        for j in range(i, i+23, 2)]
    
    return index


def get_plate_labels_full():
    """Returns the plate labels for 4x96 stamped 384 plate.  Similar to 
    get_plate_labels but returns the list for a full 384-well plate.
    """
    
    indexRanges = {1:range(0,337, 48), 2:range(1,338, 48), \
    3:range(25,362, 48), 4:range(24,361, 48)}
    
    letters = string.ascii_uppercase[0:16]
    numbers = map(str, range(1,24 + 1))
    full_labels = [(i + j) for i in letters for j in numbers]

    splitLabels = []

    # make a list for each of the four stamps
    index1 = [(full_labels[j]) for i in indexRanges[1] for j in range(i, i+23, 2)]
    index2 = [(full_labels[j]) for i in indexRanges[2] for j in range(i, i+23, 2)]
    index3 = [(full_labels[j]) for i in indexRanges[3] for j in range(i, i+23, 2)]
    index4 = [(full_labels[j]) for i in indexRanges[4] for j in range(i, i+23, 2)]    

    # put plates one and two together
    for i in range(0,8):
        curRow = index1[i*12:(i*12)+12] + index2[i*12:(i*12)+12]
        splitLabels += curRow

    # add in plates 3 and 4 together
    for i in range(0,8):
        curRow = index4[i*12:(i*12)+12] + index3[i*12:(i*12)+12]
        splitLabels += curRow
        
    return splitLabels

def fit_rois(image, manager, rowCoords, colCoords):
    """ Takes a set of ROI's from the roi manager and fits them to circles
    Also adds labels and removes out of bound rois.
    """
    
    roiCount = manager.getCount()

    for i in range(roiCount):
        manager.select(0)  # select roi from top of the list
        IJ.run("Fit Circle")
        curRoi = image.getRoi()
        coords = get_roi_center(curRoi.getBounds())
        location = get_location(coords, rowCoords, colCoords)
        
        if location:
            curRoi.setName(str(location))
            manager.runCommand("Add") # add new roi to manager
          
        
        manager.select(0) # select the top of the list again
        manager.runCommand("Delete") # delete the old roi


def analyze_particles(image, minsize, maxsize, mincirc, maxcirc, stack=False):
    """ Runs the particle analyzer on the binary processed image """

    options = PA.ADD_TO_MANAGER 
    measurements = PA.AREA + PA.STACK_POSITION + PA.FERET
    rt = ResultsTable()
    p = PA(options, measurements, rt, minsize, maxsize, mincirc, maxcirc)
    p.setHideOutputImage(True) 

    if stack:
        for i in range(image.getStackSize()):
            image.setSliceWithoutUpdate(i + 1)
            p.analyze(image)
    else:
        p.analyze(image)


def make_labels(numRow, numCol):
    """Function to make a list of well labels for a plate given the
    number of rows and columns.  Returns a list of labels. """

    """unit tested"""
    
    letters = string.ascii_uppercase[0:numRow]
    numbers = map(str, range(1,numCol + 1))

    return [(i + j) for i in letters for j in numbers]


def auto_crop(image):
    """ Function that takes an ImagePlus image of a phenotype plate and 
    attempts to autocrop it to the colonies.  Works if the image has been 
    taken with large contrast difference between the colony area and background"""

    image2 = Duplicator().run(image)
    
    # for now only works with black background
    # TODO: figure out colour of background and threshold accordingly

    IJ.run(image2,"Auto Threshold", "method=Mean white")
    
    # clean up spurious regions
    for i in range(1,20):
        IJ.run(image2,"Erode", "")
    
    ip = image2.getProcessor()

    w = Wand(ip)

    height = ip.getHeight()
    width = ip.getWidth()
    center = (width/2, height/2)

    # this step uses a wand set at the center of the image to outline the
    # colony area with an roi
    w.autoOutline(center[0], center[1], 1, 255, w.EIGHT_CONNECTED)

    roi = PolygonRoi(w.xpoints, w.ypoints, w.npoints, Roi.TRACED_ROI)
        
    image.setRoi(roi)

    if roi.getNCoordinates() != 0:
        IJ.run(image,"To Bounding Box", "")
        IJ.run(image, "Crop", "")
        #IJ.run(image, "Enhance Contrast", "saturated=0.35")


def set_scale(image):
    """ Function to set the scale on images acquired with the AlphaInnotech
    HD2 gel doc """

    cal = image.getCalibration()
    cal.pixelWidth = 0.0737687
    cal.pixelHeight = 0.0737687
    cal.pixelDepth = 0.0737687
    cal.setUnit("mm")

    image.setCalibration(cal)
    image.repaintWindow()


def get_colony_centers(image, phenotype):
    """Takes a cropped and processed image and finds all the colony centers.
    As of now this will be done for rhamnolipid images but will be adapted for 
    other ones in the future"""

    # threshold
    # this is the part that will be different for each image
    image2 = Duplicator().run(image)
    if phenotype == "Rhamnolipid":
        IJ.run(image2, "Auto Threshold", "method=MaxEntropy")
    elif phenotype == "Siderophore" or phenotype == "Hemolysis":
        IJ.run(image2,"Auto Local Threshold", "method=Niblack radius=60 parameter_1=0 parameter_2=0 white")
        IJ.run(image2,"Open", "")
        IJ.run(image2, "Fill Holes", "") 
        for i in range(1,5):
            IJ.run(image2,"Erode", "")
        IJ.run(image2, "Watershed", "")    
    else:
        return None

    # reset the scale so the results table will return pixel units
    IJ.run(image2,"Set Scale...", "distance=0 known=0 pixel=1 unit=pixel")

    # make sure we only get the colony centers
    for i in range(1,5):
        IJ.run(image2,"Erode", "")
    
    #image2.show()

    # use particle analyzer
    MAXSIZE = 100000
    MINSIZE = 1
    MINCIRC = 0.3
    MAXCIRC = 1.0
    
    options = PA.SHOW_NONE 
    measurements = PA.CENTER_OF_MASS
    
    rt = ResultsTable()
    
    p = PA(options, measurements, rt, MINSIZE, MAXSIZE, MINCIRC, MAXCIRC)
    
    p.setHideOutputImage(True)
    p.analyze(image2)

    # get the results from the results table
    x_coord = rt.getColumn(rt.getColumnIndex("XM"))
    y_coord = rt.getColumn(rt.getColumnIndex("YM"))
    colony_coords = [(x_coord[i],y_coord[i]) for i in range(0,len(x_coord))]

    image2.close()

    return colony_coords


def measure_image(image, numRow, numCol, measureType):
    """Takes an ImagePlus and returns a measurment for each spot in the
    image. The image is divided up by numRow and numCol.  measureType is a 
    field value from Image Measurements.  Mean and Area Fraction are the only
    two type needed and therefore supported for now """

    """ unit tested for both mean and areaFraction """

    if measureType == "mean":
        measureVal = Measurements.MEAN
    elif measureType == "areaFraction":
        measureVal = Measurements.AREA_FRACTION
    else:
        IJ.error("Unsuported measurement type for measure_image")
        sys.exit(1)

    # get image info and set parameters
    calibration = image.getCalibration()
    ip = image.getProcessor()
    imgHeight = ip.getHeight()
    imgWidth = ip.getWidth()
    spotHeight = imgHeight / numRow
    spotWidth = imgWidth / numCol

    # well labels and grid 
    labels = make_labels(numRow, numCol)
    grid = make_grid(spotWidth, spotHeight, numRow, numCol)
    
    # measure the area fraction in each location
    measurements = []
    for spot in labels:
        x,y,width,height = grid[spot]
        roi = Roi(x, y, width, height)
        roi.setName(spot)
        ip.setRoi(roi)
        stats = ImageStatistics.getStatistics(ip, measureVal, calibration)
        
        # get the correct measurment frm the ImageStatistics object
        if measureVal == Measurements.MEAN:
            value = stats.mean
        elif measureVal == Measurements.AREA_FRACTION:
            value = stats.areaFraction
        else:
            value = 0
        
        measurements.append((spot, int(round(value))))

    return measurements


def hcn_process(image):
    """  Function that runs the pre-processing steps on an HCN image """

    IJ.run(image, "Median...", "radius=5");
    IJ.run(image, "Subtract Background...", "rolling=50 light");
    IJ.run(image, "8-bit", "")
    IJ.run(image, "Invert", "")


def ab_process(image):
    """ Function that runs the pre-processing steps on an antibiotic image"""

    IJ.run(image, "Enhance Contrast...", "saturated=0.4")
    IJ.run(image, "8-bit", "")
    IJ.run(image, "Subtract Background...", "rolling=50")
    IJ.run(image, "Auto Threshold", "method=Triangle white")
    IJ.run(image, "Open", "")

def drawBoundBox(image, width, height):
    """ Function that draws a bounding box around the colonies sized with 
    the given width and height"""

    # get some image info
    ip = image.getProcessor()
    imgHeight = ip.getHeight()
    imgWidth = ip.getWidth()


    # position it in the centre of the image
    y = (imgHeight/2) - (height/2)
    x = (imgWidth/2) - (width/2)

    image.setRoi(Roi(x, y, width, height))


def get_measurements(image, measureType):
    """ Takes the list of ROI's from the ROI manager, assigns well labels 
    based on user given plate paramters and returns a set of measurements. 
    measureType is the measurement being recorded. Can be 'noSwim', 
    'phenotype', or 'swim'. """

    # get some info about the image and set up parameters
    imageName = image.getShortTitle()
    if measureType == "noSwim":
        resultsTitle = imageName + "_results_nonSwim"
    else:
        resultsTitle = imageName + "_results"
    
    # intialize a new results table
    Rt = ResultsTable()
    Rt.setPrecision(2)

    # extract the info from file name, requires a specific naming structure
    infoRegex = re.compile("(.*)-(\D*)(\d*)-(.*)")
    info = infoRegex.search(imageName)
    if info is not None and len(info.groups()) == 4 and measureType == "phenotype":
        sample, media, replicate, stamp = info.groups()
        stamp = stamp[1]  # get the digit part of the stamp
    else:
        sample, media, replicate, stamp = "unknown", "unknown", 1, 1

    # get plate type and set up grid coordinates
    print sample, media, replicate, stamp

    spotLabels, numRow, numCol, sample, media, replicate  \
        = get_options((int(stamp)-1), sample, media, str(replicate))


    rowCoords, colCoords = make_grid_location(image, spotLabels, numRow, numCol)

    # get measurements
    # ------------------

    manager = RoiManager.getInstance()
    roiList = manager.getRoisAsArray()
    
    for roi in roiList:
        image.setRoi(roi)
        diameter = roi.getFeretsDiameter()
        position = roi.getPosition()
        bounds = roi.getBounds()
        center = get_roi_center(bounds)
        label = get_location(center, rowCoords, colCoords)

        # add the value to the results table
        if label:
            Rt.incrementCounter()
            Rt.addValue("Diameter", diameter)
            Rt.addValue("Sample", sample)
            Rt.addValue("Media", media)
            Rt.addValue("Replicate", replicate)
            if measureType == "swim": Rt.addValue("Time", position)
            Rt.addLabel(label)
            roi.setName(label)
        else:
            continue 

    Rt.show(resultsTitle)


def prepRoiManager():
    """ Open ROI manager if not open and remove all current ROIs 
        from manager if it is open
    """ 

    manager = RoiManager.getInstance()
    if manager == None:
        manager = RoiManager()
    else:   
        manager.runCommand("Select All")
        manager.runCommand("Delete")
            
    return manager


def getGridSize():
    
    gd = GenericDialog("Grid Size")
    gd.addNumericField("Rows", 16, 0)  
    gd.addNumericField("Columns", 24, 0)
    gd.showDialog()
    if gd.wasCanceled():
            return

    numRow = gd.getNextNumber()
    numCol = gd.getNextNumber()

    return numRow, numCol


def ROI_grid(image, numRow, numCol):
    """ Function to construct an ROI grid with the given dimensions."""

    ip = image.getProcessor()

 
    box_height = ip.getHeight()
    box_width = ip.getWidth()
    x = 0
    y = 0

    # get the grid box dimensions
    height = box_height / numRow
    width = box_width / numCol


    numRow = int(numRow)
    numCol = int(numCol)        

    m = prepRoiManager()
    
    # make the labels based on number of columns and rows   
    letters = string.ascii_uppercase[0:numRow]
    numbers = map(str, range(1,numCol+1))

    for i in range(0, numRow):
        for j in range(0, numCol):
            
            xOffset = j * width
            yOffset = i * height
            currentWell = letters[i] + numbers[j]
            roi = Roi(x + xOffset, y + yOffset, width, height)
            image.setRoi(roi)
            m.addRoi(roi)
            m.select(m.getCount() - 1)
            m.runCommand("Rename", currentWell) 


def get_phenotype():
    """ Gets the phenotype from the user with a dialog.  Also includes an
    option to use the colony detector"""

    phenotypes = ("Protease", "Siderophore", "Hemolysis", "Rhamnolipid", "Don't pre-process")
    gd = GenericDialog("Select pre-process type")
    gd.addRadioButtonGroup("", phenotypes, 5, 1, phenotypes[0])
    gd.addCheckbox("Attempt to find colonies:", False)
    gd.showDialog()
    if gd.wasCanceled():
        sys.exit()
    return gd.getNextRadioButton(), gd.getNextBoolean()


def get_threshold_parameter(radius_default, type_default):
    """Gets the parameter for auto local thresholding"""
    gd = GenericDialog("Select threshold parameters")
    gd.addNumericField("Radius:", radius_default, 0)
    types = ("Bernsen", "Phansalkar")
    gd.addRadioButtonGroup("", types, 1,2,types[type_default])
    gd.showDialog()
    if gd.wasCanceled():
        sys.exit()
    return gd.getNextNumber(), gd.getNextRadioButton()


def protease_process(image):
    """ Function that takes a reference to ImagePlus image and performs
    pre-processing/filtering prior to peak detection, optimized for protease
    image types.
    """
    radius, threshold = (50, "Phansalkar")

    thresholdParam = "method=%s radius=%s parameter_1=0 parameter_2=0 white" % \
        (threshold, radius)

    # bandpass filter evens out the illumination
    IJ.run(image, "Bandpass Filter...", \
       "filter_large=500 filter_small=0 suppress=None tolerance=5 autoscale saturate")

    # median filter removes artifacts, smooths out the image
    #IJ.run(image, "Fast Filters", "link filter=median x=5 y=5 preprocessing=none offset=128 stack")
    IJ.run(image, "Median...", "radius=5")
    IJ.run(image, "Enhance Local Contrast (CLAHE)", "blocksize=300 histogram=256 maximum=2 mask=*None*")
    IJ.run(image,"Auto Local Threshold", thresholdParam )
    IJ.run(image, "Find Edges", "")


def siderophore_process(image):
    """ Function that takes a reference to ImagePlus image and performs
    pre-processing/filtering prior to peak detection, optimized for siderophore
    image types.
    """

    radius, threshold = (60, "Bernsen")
    thresholdParam = "method=%s radius=%s parameter_1=50 parameter_2=0 white" % \
        (threshold, radius)
    
    # bandpass filter evens out the illumination
    IJ.run(image, "Bandpass Filter...", \
       "filter_large=250 filter_small=0 suppress=None tolerance=5 autoscale saturate")
    IJ.run(image, "Mean...", "radius=5")
    IJ.run(image, "Median...", "radius=10")
    #IJ.run(image, "Fast Filters", "link filter=mean x=5 y=5 preprocessing=none offset=128 stack")
    #IJ.run(image, "Fast Filters", "link filter=median x=10 y=10 preprocessing=none offset=128 stack")
    IJ.run(image, "Enhance Local Contrast (CLAHE)", "blocksize=300 histogram=256 maximum=2 mask=*None*")
    IJ.run(image,"Auto Local Threshold", thresholdParam)
    IJ.run(image, "Find Edges", "")


def hemolysis_process(image):
    """ Function that takes a reference to ImagePlus image and performs
    pre-processing/filtering prior to peak detection, optimized for hemolysis
    image types.
    """
    radius, threshold = (60, "Phansalkar")
    thresholdParam = "method=%s radius=%s parameter_1=0.01 parameter_2=1 white" % \
        (threshold, radius)
    
    # bandpass filter evens out the illumination
    IJ.run(image, "Bandpass Filter...", \
       "filter_large=250 filter_small=0 suppress=None tolerance=5 autoscale saturate")
    IJ.run(image,"Subtract Background...", "rolling=75 light sliding")
    IJ.run(image, "Mean...", "radius=5")
    IJ.run(image, "Median...", "radius=10")
    #IJ.run(image, "Fast Filters", "link filter=mean x=5 y=5 preprocessing=none offset=128 stack")
    #IJ.run(image, "Fast Filters", "link filter=median x=10 y=10 preprocessing=none offset=128 stack")
    IJ.run(image, "Enhance Local Contrast (CLAHE)", "blocksize=300 histogram=256 maximum=2 mask=*None*")
    IJ.run(image,"Auto Local Threshold", thresholdParam)
    IJ.run(image, "Find Edges", "")


def rhamnolipid_process(image):
    """ Function that takes a reference to ImagePlus image and performs
    pre-processing/filtering prior to peak detection, optimized for rhamnolipid"""
    radius, threshold, p1 = (10, "Bernsen", 11)
    IJ.run(image,"Subtract Background...", "rolling=100 light sliding")
    thresholdParam = "method=%s radius=%s parameter_1=%s parameter_2=0" % (threshold, radius, p1)
    IJ.run(image,"Auto Local Threshold", thresholdParam)
    IJ.run(image,"Close-", "")
    IJ.run(image, "Fill Holes", "")
    IJ.run(image, "Find Edges", "")
    IJ.run(image, "Dilate", "")

def run_profiles():
    """ This function does most of the work to detect the different phenotype
    profiles.  Using the active image it runs the appropriate pre-processing 
    based on user selection.  This is followed by peak detection and resulting
    roi's are loaded into the roi manager.
    """

    # get the open image and quit if not 8-bit
    image = WindowManager.getCurrentImage()
    if image.type != ImagePlus.GRAY8:
        IJ.error("Please convert to 8-bit first")
        sys.exit(1)

    # duplicate 
    image2 = Duplicator().run(image)
    processType, useColonies = get_phenotype()


    # get some image dimensions and setup the grid and labels
    numRow = 8
    numCol = 12
    ip = image2.getProcessor()
    imgHeight = ip.getHeight()
    imgWidth = ip.getWidth()
    height = imgHeight / numRow
    width = imgWidth / numCol

    labels = make_labels(numRow, numCol)
    grid = make_grid(width, height, numRow, numCol)

    # Let's find the colonies if the user specifies, otherwise
    # just use the center of the grid roi

    if useColonies:
        colonies = get_colony_centers(image2, processType)
    
    if useColonies == False or colonies == None:
        colonies = []
        for well,spot in grid.iteritems():
            centerX = spot[0] + (spot[2] / 2)   # center x coord
            centerY = spot[1] + (spot[3] / 2)   # center y coord
            colonies.append((centerX, centerY))      

    # process the image according to type
    if processType == "Protease":
        protease_process(image2)
    elif processType == "Siderophore":
        siderophore_process(image2)
    elif processType == "Hemolysis":
        hemolysis_process(image2)
    elif processType == "Rhamnolipid":
        rhamnolipid_process(image2)

    # setup the ROI manager
    if RoiManager.getInstance() is None:
        manager = RoiManager()
    else:
        manager = RoiManager.getInstance()


    # this uses colony centers to find zones
    # =============================================
    
    for colony in colonies:

        # draw an roi line across the colony and zone
        start_x = colony[0] - width/2
        start_y = colony[1] - height/2
        end_x = colony[0] + width/2
        end_y = colony[1] + height/2
        roi = Line(start_x, start_y, end_x, end_y)

        # find the peaks
        image2.setRoi(roi)
        profile = roi.getPixels()
        peakProf = PeakProfile(profile)

        # if there is nothing get out
        if len(peakProf.peaks) <= 1:
            continue

        # get the colony and zone peaks
        mid = median(peakProf.peaks) + 1
        colonyPeaks, zonePeaks = peakProf.get_pairs(mid)

        # colony and zone detected
        if colonyPeaks and zonePeaks:
            diameter = calc_diameter(colonyPeaks, zonePeaks)

        # only colony detected
        if colonyPeaks and not zonePeaks:
            colonySize = abs(colonyPeaks[0]-colonyPeaks[1])

            # Set the diameter to zero if the colony size is less than
            # 50 pixels.  Hopefully this will reduce false positives.
            if  colonySize < 50:
                diameter = 0
            else:
                diameter = colonySize
    
        if colonyPeaks:
            if useColonies:
                zone_x = colony[0] - diameter/2
                zone_y = colony[1] - diameter/2
            else:
                zone_x, zone_y = find_zone_coords(colonyPeaks, start_x, start_y, diameter)
            zone = OvalRoi(zone_x, zone_y, diameter, diameter)
            image.setRoi(zone)
            manager.addRoi(zone)

    manager.runCommand("Show All")            
    manager.runCommand("Show All with labels") 