from phenotype.phenotype import *
import unittest

class PhenotypesTestCase(unittest.TestCase):
    """Tests for phenotypes code"""

    def test_make_grid(self):
        """ Does make_grid return a valid grid?"""

        # width: 2, height: 2, rows: 2, cols: 2
        good_grid = {'A1':(0,0,2,2), 'A2':(2,0,2,2),
                     'B1':(0,2,2,2), 'B2':(2,2,2,2)}

        self.assertEqual(make_grid(2,2,2,2), good_grid)


    def test_make_labels(self):
        """ Does make_labels return valid labels? """

        good_labels = ['A1', 'A2', 'A3', 'A4', 
                       'B1', 'B2', 'B3', 'B4',
                       'C1', 'C2', 'C3', 'C4']

        self.assertEqual(make_labels(3,4), good_labels)


    def test_measure_image_mean(self):
        """ Does measure image correctly return the mean value? """

        # make a test image filled with grey
        imp = IJ.createImage("areaFraction test image", "8-bit", 512, 512, 1)
        ip = imp.getProcessor()
        width, height = 512/4, 512/4
        curGrey = 0
        for i in range(0,4):
            for j in range(0,4):
                x = j*width
                y = i*height
                roi = Roi(x,y,width, height)
                ip.setRoi(roi)
                ip.setValue(curGrey)
                ip.fill()
                curGrey += 10

        good_measurements = [('A1',0.0), ('A2', 10.0), ('A3', 20.0), ('A4', 30.0),
                             ('B1',40.0), ('B2', 50.0), ('B3', 60.0), ('B4', 70.0),
                             ('C1',80.0), ('C2', 90.0), ('C3', 100.0), ('C4', 110.0),
                             ('D1',120.0), ('D2', 130.0), ('D3', 140.0), ('D4', 150.0)]

        self.assertEqual(measure_image(imp, 4, 4, "mean"), good_measurements)



    def test_measure_image_areaFraction(self):
        """ Does measure image correctly return the areaFraction value? """
        
        # make a test image with spots of diff size
        imp = IJ.createImage("mean test image", "8-bit", 512, 512, 1)
        ip = imp.getProcessor()
        width, height = 512/4, 512/4
        ip.setValue(0)
        ip.fill()
        IJ.setForegroundColor(255, 255, 255)
        offset = 20
        for i in range(0,4):
            for j in range(0,4):
                x = j*width
                y = i*height
                cenX = x + (width/2)
                cenY = y + (height/2)
                roiWidth = width - offset
                roiHeight = height - offset
                roiX = cenX - (roiWidth/2)
                roiY = cenY - (roiHeight/2)
                roi = OvalRoi(roiX, roiY, roiWidth, roiHeight)
                imp.setRoi(roi)
                IJ.run(imp, "Fill", "")
                offset += 5

        good_measurements = [('A1',56), ('A2', 51), ('A3', 46), ('A4', 41),
                             ('B1',37), ('B2', 33), ('B3', 29), ('B4', 26),
                             ('C1',22), ('C2', 19), ('C3', 16), ('C4', 14),
                             ('D1',11), ('D2', 9), ('D3', 7), ('D4', 5)]

        self.assertEqual(measure_image(imp, 4, 4, "areaFraction"),
                             good_measurements)


    def test_make_grid_location(self):
        """ Does make_grid_location correctly return valid labels? """


        # make a test image because this function takes an image
        imp = IJ.createImage("areaFraction test image", "8-bit", 512, 512, 1)

        # need some spot labels, with 3 rows and 4 columns
        spotLabels = ['A1', 'A2', 'A3', 'A4', 
                      'B1', 'B2', 'B3', 'B4',
                      'C1', 'C2', 'C3', 'C4',
                      'D1', 'D2', 'D3', 'D4']
        
        numRow, numCol = 4, 4

        # and now for some good output
        goodRowCoords = {'A':(0,127), 'B':(128,255), 'C':(256,383), 'D':(384,511)}
        goodColCoords = {'1':(0,127), '2':(128,255), '3':(256,383), '4':(384,511)}

        self.assertEqual(make_grid_location(imp, spotLabels, numRow, numCol),
            (goodRowCoords, goodColCoords))


    def test_make_grid_location_modifier(self):
        """ Does make_grid_location correctly use the modifier? """


        # make a test image because this function takes an image
        imp = IJ.createImage("areaFraction test image", "8-bit", 512, 512, 1)

        # need some spot labels, with 3 rows and 4 columns
        spotLabels = ['A1', 'A2', 'A3', 'A4', 
                      'B1', 'B2', 'B3', 'B4',
                      'C1', 'C2', 'C3', 'C4',
                      'D1', 'D2', 'D3', 'D4']
        
        numRow, numCol = 4, 4
        modifier = 1

        # and now for some good output
        goodRowCoords = {'A':(1,126), 'B':(129,254), 'C':(257,382), 'D':(385,510)}
        goodColCoords = {'1':(1,126), '2':(129,254), '3':(257,382), '4':(385,510)}

        self.assertEqual(make_grid_location(imp, spotLabels, numRow, numCol, modifier),
            (goodRowCoords, goodColCoords))

    def test_get_location(self):
        """ Does get_location return the correct location? """

        rowCoords = {'A':(1,126), 'B':(129,254), 'C':(257,382), 'D':(385,510)}
        colCoords = {'1':(1,126), '2':(129,254), '3':(257,382), '4':(385,510)}
        pointCoords = (130, 130)
        goodValue = 'B2'

        self.assertEqual(get_location(pointCoords, rowCoords, colCoords), goodValue)

if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(PhenotypesTestCase)
    unittest.TextTestRunner(verbosity=2).run(suite)